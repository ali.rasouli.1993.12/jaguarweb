<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Models\Category;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\TagsController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProjectController;
use App\Http\Controllers\Admin\ClientsController;
use App\Http\Controllers\Admin\FromController;
use App\Http\Controllers\SitemapXmlController;
use App\Http\Controllers\CaptchaServiceController;
use App\Http\Controllers\BlogController as mainBlogController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware([checkAdmin::class])->group(function() {
    Route::group(['prefix' => 'dashboard', 'namespace' => 'Admin'], function () {
        Route::get('/', [DashboardController::class, 'dashboard'])->name('admin.dashboard');

        /*
        * blogs routes
        */
        Route::group(['prefix' => 'Blog'], function () {
            Route::get('/', [BlogController::class, 'index'])->name('admin.blog.index');
            Route::get('/create', [BlogController::class, 'create'])->name('admin.blog.create');
            Route::post('/insert', [BlogController::class, 'insert'])->name('admin.blog.insert');
            Route::get('/edit/{id}', [BlogController::class, 'edit'])->name('admin.blog.edit');
            Route::post('/update/{id}', [BlogController::class, 'update'])->name('admin.blog.update');
            Route::get('/delete/{id}', [BlogController::class, 'delete'])->name('admin.blog.delete');
        });

        /*
        * Project
        */
        Route::prefix('Project')->group(function () {
            Route::get('/', [ProjectController::class, 'index'])->name('admin.project.index');
            Route::get('/create', [ProjectController::class, 'create'])->name('admin.project.create');
            Route::post('/insert', [ProjectController::class, 'insert'])->name('admin.project.insert');
            Route::get('/edit/{id}', [ProjectController::class, 'edit'])->name('admin.project.edit');
            Route::post('/update/{id}', [ProjectController::class, 'update'])->name('admin.project.update');
            Route::get('/delete/{id}', [ProjectController::class, 'delete'])->name('admin.project.delete');
        });

        /*
        * Client
        */
        Route::prefix('Client')->group(function () {
            Route::get('/', [ClientsController::class, 'index'])->name('admin.client.index');
            Route::get('/create', [ClientsController::class, 'create'])->name('admin.client.create');
            Route::post('/insert', [ClientsController::class, 'store'])->name('admin.client.insert');
            Route::get('/edit/{id}', [ClientsController::class, 'edit'])->name('admin.client.edit');
            Route::post('/update/{id}', [ClientsController::class, 'update'])->name('admin.client.update');
            Route::get('/delete/{id}', [ClientsController::class, 'destroy'])->name('admin.client.delete');
        });

        /*
        * tags routes
        */
        Route::group(['prefix' => 'Tag'], function () {
            Route::get('/', [TagsController::class, 'index'])->name('admin.tag.index');
            Route::get('/Blog', [TagsController::class, 'listBlog'])->name('admin.tag.blog.list');
            Route::get('/Project', [TagsController::class, 'listProject'])->name('admin.tag.project.list');
            /*
            * Tag Blog
            */
            Route::group(['prefix' => 'Blog'], function () {
                Route::get('create', [TagsController::class, 'createBlog'])->name('admin.tag.blog.create');
                Route::post('store', [TagsController::class, 'storeBlog'])->name('admin.tag.blog.store');
                Route::get('edit/{id}', [TagsController::class, 'editBlog'])->name('admin.tag.blog.edit');
                Route::post('update/{id}', [TagsController::class, 'updateBlog'])->name('admin.tag.blog.update');
                Route::get('delete/{id}', [TagsController::class, 'deleteBlog'])->name('admin.tag.blog.delete');
            });

            /*
            * Tag Project
            */
            Route::group(['prefix' => 'Project'], function () {
                Route::get('create', [TagsController::class, 'createProject'])->name('admin.tag.project.create');
                Route::post('store', [TagsController::class, 'storeProject'])->name('admin.tag.project.store');
                Route::get('edit/{id}', [TagsController::class, 'editProject'])->name('admin.tag.project.edit');
                Route::post('update/{id}', [TagsController::class, 'updateProject'])->name('admin.tag.project.update');
                Route::get('delete/{id}', [TagsController::class, 'deleteProject'])->name('admin.tag.project.delete');
            });
        });

        /*
        * Category routes
        */
        Route::prefix('Category')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('admin.category.index');
            Route::get('/Blog', [CategoryController::class, 'listBlog'])->name('admin.category.blog.list');
            Route::get('/Project', [CategoryController::class, 'listProject'])->name('admin.category.project.list');

            //Blogs
            Route::prefix('Blog')->group(function () {
                Route::get('create', [CategoryController::class, 'createBlog'])->name('admin.category.blog.create');
                Route::post('store', [CategoryController::class, 'storeBlog'])->name('admin.category.blog.store');
                Route::get('edit/{id}', [CategoryController::class, 'editBlog'])->name('admin.category.blog.edit');
                Route::post('update/{id}', [CategoryController::class, 'updateBlog'])->name('admin.category.blog.update');
                Route::get('delete/{id}', [CategoryController::class, 'deleteBlog'])->name('admin.category.blog.delete');
            });
            //Projects
            Route::prefix('Project')->group(function () {
                Route::get('create', [CategoryController::class, 'createProject'])->name('admin.category.project.create');
                Route::post('store', [CategoryController::class, 'storeProject'])->name('admin.category.project.store');
                Route::get('edit/{id}', [CategoryController::class, 'editProject'])->name('admin.category.project.edit');
                Route::post('update/{id}', [CategoryController::class, 'updateProject'])->name('admin.category.project.update');
                Route::get('delete/{id}', [CategoryController::class, 'deleteProject'])->name('admin.category.project.delete');
            });
        });

        /*
        * forms
        */
        Route::prefix('Form')->group(function () {
            Route::get('/', [FromController::class, 'index'])->name('admin.form.index');
            Route::get('/show/{id}', [FromController::class, 'show'])->name('admin.form.show');
            Route::get('/delete/{id}', [FromController::class, 'destroy'])->name('admin.form.delete');
        });
    });
    Route::get('/logout/{id}', [AuthController::class, 'logout'])->name('logout');
});
Route::middleware('guest')->group(function() {
    Route::get('/login', [AuthController::class, 'showLogin'])->name('showLogin');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::get('/register', [AuthController::class, 'showRegister'])->name('showRegister');
    Route::post('/register', [AuthController::class, 'register'])->name('register');
});


Route::get('/', [HomeController::class, 'home'])->name('home');
Route::post('/blog/search', [HomeController::class, 'search'])->name('search');
Route::get('/blog', [HomeController::class, 'blog'])->name('blog');
Route::get('/blog/{blog}', [HomeController::class, 'singleBlog'])->name('singleBlog');
Route::get('/category/{slug}', [mainBlogController::class, 'showCategory'])->name('showCategory');
Route::get('/portfolio', [HomeController::class, 'portfolio'])->name('portfolio');
Route::get('/portfolio/{slug}', [HomeController::class, 'singlePortfolio'])->name('singlePortfolio');

Route::post('uploads', [BlogController::class, 'uploadImage'])->name('posts.upload');
Route::get('sitemap.xml', [SitemapXmlController::class, 'index'])->name('sitemap');

Route::post('/captcha-validation', [CaptchaServiceController::class, 'capthcaFormValidate'])->name('form');
Route::get('/reload-captcha', [CaptchaServiceController::class, 'reloadCaptcha']);

Route::get('/cacaca', function(){
    $a = Artisan::call('config:cache') ;
    echo $a;
});
