<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->text('slug')->unique();
            $table->string('title');
            $table->string('imageURL');
            $table->text('body');
            $table->string('shortDescription')->nullable();
            $table->text('tagsId')->nullable();
            $table->string('categoryId');
            $table->string('status');
            $table->string('avgRate')->default(0);
            $table->string('countRate')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
