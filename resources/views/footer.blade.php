<!-- ======= Footer ======= -->
<footer id="footer" class="footer">

    <div class="footer-content">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Jaguar Web</h3>
                        <p>
                            سئو سایت در واقع مسیر اصلی تبلیغات و معرفی برند شما به مخاطبانتان است. در اصل وب سایت شما شعبه آنلاین کسب و کارتان می‌باشد، شعبه ای که توانایی ارائه خدمات به مخاطبین بیشتر از هر نقطه ی ایران و حتی جهان را دارد. اما این که این شعبه در معرض دید مشتری قرار بگیرد و گذر کاربر به آن بیافتد به عهده مسیری است که Search Engine Optimization) SEO) نام دارد، در واقع بدون بهینه سازی، سایت شما مثل مغازه ای است که در کوچه پس کوچه های یک شهر متروک قرار گرفته و سالی به دوازده ماه گذر مشتری به آن نمی‌افتد. اما با جدی گرفتن و انجام اصولی سئو سایت نتیجه ای که از حضور در فضای وب می‌گیرید غیر قابل باور است.
                        </p>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>لینک مفید</h4>
                    <ul>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">میزبانی وب</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">مشاوره</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">آموزش سئو</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">آموزش برنامه نویسی</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="{{ route('sitemap') }}">سایت مپ</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>خدمات</h4>
                    <ul>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">طراحی گرافیک</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">کدنویسی</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">سئو و بهینه سازی</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">امنیت</a></li>
                        <li><i class="bi bi-chevron-left"></i> <a href="#!">طراحی سایت</a></li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>جگوار وب TV</h4>
                    <img src="{{ asset('Site/assets/img/features-2.svg') }}" alt="طراحی سایت" class="img-fluid">
                </div>

            </div>
        </div>
    </div>

    <div class="footer-legal text-center">
        <div class="container d-flex flex-column flex-lg-row justify-content-center justify-content-lg-between align-items-center">

            <div class="d-flex flex-column align-items-center align-items-lg-start">
                <div class="copyright" dir="ltr">
                    &copy; Copyright <strong><span>Jaguar Web</span></strong>. All Rights Reserved
                </div>
            </div>

            <div class="social-links order-first order-lg-last mb-3 mb-lg-0">
                <a href="https://t.me/JaguarWebIR" class="telegram" target="_blank"><i class="bi bi-telegram"></i></a>
                <a href="https://www.facebook.com/jaguarweb.ir/" class="facebook" target="_blank"><i class="bi bi-facebook"></i></a>
                <a href="https://twitter.com/jaguarweb_ir" class="twitter" target="_blank"><i class="bi bi-twitter"></i></a>
                <a href="https://www.instagram.com/jaguarweb_ir/" class="instagram" target="_blank"><i class="bi bi-instagram"></i></a>
                <a href="https://www.pinterest.com/jaguarweb" class="pinterest" target="_blank"><i class="bi bi-pinterest"></i></a>
                <a href="https://www.linkedin.com/in/jaguar-web-844aa5235/" class="linkedin" target="_blank"><i class="bi bi-linkedin"></i></a>

            </div>

        </div>
    </div>

</footer><!-- End Footer -->
