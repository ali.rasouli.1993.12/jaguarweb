@extends('layout')

@section('content')
    <div class="hero-animated">
        <section id="cta" class="cta">
            <div class="container aos-init aos-animate" data-aos="zoom-out">

                <div class="row g-5">

                    <div class="col-lg-8 col-md-6 content d-flex flex-column justify-content-center order-last order-md-first">
                        <h3>برگه مورد نظر یافت نشد یا وجود ندارد</h3>
                        <p>خطای 404 - وجود نداشتن برگه</p></div<br />
                    <a class="cta-btn align-self-start" href="{{ route('home') }}">بازگشت به خانه</a>
                </div>

                <div class="col-lg-4 col-md-6 order-first order-md-last d-flex align-items-center">
                    <div class="img">
                        <img src="{{ asset('Site/assets/img/cta.jpg') }}" alt="بازگشت به خانه" class="img-fluid">
                    </div>
                </div>

            </div>
        </section>
    </div>
@endsection
