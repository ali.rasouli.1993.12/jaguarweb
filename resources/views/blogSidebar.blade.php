<div class="sidebar">

    <div class="sidebar-item search-form">
        <h3 class="sidebar-title">جستجو</h3>
        <form action="{{ route('search') }}" method="post" class="mt-3">
            @csrf
            <input name="search" id="search" type="text">
            <button type="submit"><i class="bi bi-search"></i></button>
        </form>
    </div><!-- End sidebar search formn-->

    <div class="sidebar-item categories">
        <h3 class="sidebar-title">دسته بندی های مقالات</h3>
        <ul class="mt-3">
            @foreach($categories as $cat)
                <li>
                    <a href="{{ route('showCategory', $cat->slug) }}">{{ $cat->title }}
                    </a>
                </li>
                @if($cat->childCat->count() > 0)
                    @foreach($cat->childCat as $child)
                        <li>
                            <a style="margin-right: 20px;"
                               href="{{ route('showCategory', $child->slug) }}">{{ $child->title }}
                            </a>
                        </li>
                    @endforeach
                @endif
            @endforeach
        </ul>
    </div><!-- End sidebar categories-->

    <div class="sidebar-item recent-posts">
        <h3 class="sidebar-title">آخرین مطالب منتشر شده</h3>
        <div class="mt-3">
            @foreach($recentBlogs as $rc)
                <div class="post-item mt-3">
                    <img src="{{ url($rc->imageURL) }}" alt="{{ $rc->title }}" class="flex-shrink-0">
                    <div>
                        <h4><a href="{{ route('singleBlog', $rc->slug) }}">{{ $rc->title }}</a></h4>
                        <p>{{ \Illuminate\Support\Str::limit($rc->shortDescription, 25) }}</p>
                        <time datetime="{{ \Illuminate\Support\Carbon::createFromFormat('Y-m-d H:i:s', $rc->created_at)->format('d-m-Y') }}">
                            {{ \Morilog\Jalali\Jalalian::forge($rc->create_at)->format('%d %B %Y') }}
                        </time>
                    </div>
                </div><!-- End recent post item-->
            @endforeach
        </div>

    </div><!-- End sidebar recent posts-->

    <div class="sidebar-item tags">
        <h3 class="sidebar-title">آخرین برچسب ها</h3>
        <ul class="mt-3">
            @foreach($tags as $tag)
            <li><a href="!#">{{ $tag->title }}</a></li>
            @endforeach
        </ul>
    </div><!-- End sidebar tags-->

</div><!-- End Blog Sidebar -->
