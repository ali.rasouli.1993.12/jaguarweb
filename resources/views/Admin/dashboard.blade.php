@extends('Admin.layout')

@section('page_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ auth()->user()->name }} عزیز ، خوش آمدید</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item active">صفحه اصلی</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible col-sm-3">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-header">--}}
{{--                            <h3 class="card-title m-0" style="float: right; padding: 6px 12px;">همه ی بلاگ ها</h3>--}}
{{--                            <a href="#" class="btn btn-primary" style="float: left">افزودن پست جدید</a>--}}
{{--                        </div>--}}
{{--                        <!-- /.card-header -->--}}
{{--                        <div class="card-body">--}}
{{--                            <div class="row">--}}
{{--                                @if($allBlogs->count() > 0)--}}
{{--                                    <table id="example2" class="table table-bordered table-hover">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th>ردیف</th>--}}
{{--                                            <th>عنوان پست / مقاله</th>--}}
{{--                                            <th>توضیح کوتاه</th>--}}
{{--                                            <th>وضعیت</th>--}}
{{--                                            <th>عملیات</th>--}}
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--                                        @foreach($allBlogs as $blog)--}}
{{--                                            <tr>--}}
{{--                                                <td>{{ $loop->iteration }}</td>--}}
{{--                                                <td>{{ $blog->title }}</td>--}}
{{--                                                <td>{{ $blog->body }}</td>--}}
{{--                                                <td>{{ $blog->status }}</td>--}}
{{--                                                <td>ویرایش / حذف</td>--}}
{{--                                            </tr>--}}
{{--                                        @endforeach--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                @else--}}
{{--                                    <p class="alert alert-danger p-2">هیچ پست یا مقاله ای در سایت موجود نیست</p>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Dashboard > a').addClass('active');
    </script>
@endsection
