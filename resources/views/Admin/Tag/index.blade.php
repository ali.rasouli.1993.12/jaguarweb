@extends('Admin.layout')
@section('title')برچسب ها@endsection
@section('page_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">برچسب ها</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item active">برچسب ها</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible col-sm-3">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title m-0" style="float: right; padding: 6px 12px;">انتخاب برچسب</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="card text-center">
                                        <a href="{{ route('admin.tag.blog.list') }}">
                                            <i class="nav-icon fas fa-list-alt mt-2" style="font-size: 5rem"></i>
                                            <p style="font-size: 3rem">
                                                بلاگ
                                            </p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="card text-center">
                                        <a href="{{ route('admin.tag.project.list') }}">
                                            <i class="nav-icon fas fa-home mt-2" style="font-size: 5rem"></i>
                                            <p style="font-size: 3rem">
                                                پروژه
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Tag > a').addClass('active');
    </script>
@endsection
