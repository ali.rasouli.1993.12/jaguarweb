@extends('Admin.layout')
@section('title')افزودن برچسب@endsection
@section('page_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">برچسب ها</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.tag.project.list') }}">لیست برچسب ها</a></li>
                <li class="breadcrumb-item active">افزودن برچسب</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible col-sm-3">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title m-0" style="float: right; padding: 6px 12px;">افزودن برچسب</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form class="form-horizontal row" action="{{ route('admin.tag.project.store') }}" method="post">
                                        @csrf
                                        <div class="col-sm-4">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label" for="title">نام برچسب</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="title" id="title" placeholder="نام برچسب">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group row">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="status" name="status">
                                                        <label class="form-check-label" for="status">وضعیت انتشار</label>
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary" type="submit">ذخیره</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Tag-Project > a').addClass('active');
    </script>
@endsection
