{{--allTagListProject--}}
@extends('Admin.layout')

@section('title')برچسب های پروژه@endsection

@section('page_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">برحسب های پروژه</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item active">برحسب های پروژه</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible col-sm-3">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title m-0" style="float: right; padding: 6px 12px;">برچسب های پروژه</h3>
                            <a href="{{ route('admin.tag.project.create') }}" class="btn btn-primary" style="float: left">افزودن برچسب جدید</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                @if($allTagListProject->count() > 0)
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان</th>
                                            <th>وضعیت</th>
                                            <th style="width: 200px">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($allTagListProject as $tag)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $tag->title }}</td>
                                                <td>{!! $tag->returnLabelStatus($tag->status) !!}</td>
                                                <td class="d-flex" style="width: 200px">
                                                    <a href="{{ route('admin.tag.project.edit', $tag->id) }}" class="btn-info btn-sm ml-2">ویرایش</a>
                                                    <a href="{{ route('admin.tag.project.delete', $tag->id) }}" class="btn-danger btn-sm">حذف</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $allTagListProject->links('Admin.pagination') }}
                                @else
                                    <p class="alert alert-danger p-2">هیچ برچسبی در سایت موجود نیست</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Tag-Project > a').addClass('active');
    </script>
@endsection
