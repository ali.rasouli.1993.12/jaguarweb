@extends('Admin.layout')

@section('title')فرم@endsection()

@section('page_header')
    <div class="row">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">فرم</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item active">فرم</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection

@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">نام و نام خانوادگی:
                                            <b>
                                                {{ $form->name }}
                                            </b>
                                        </div>
                                        <div class="col-sm-12 col-md-6">شماره موبایل:
                                            <b>{{ $form->mobile }}</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">ایمیل:
                                            <b>
                                                {{ $form->email }}
                                            </b>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
{{--                                            <b>{{ $form->mobile }}</b>--}}
                                        </div>
                                    </div>
                                </div>
                                <hr style="height:1px; width:100%; border-width:0; color:lightgray; background-color:lightgray">
                                <div class="col-sm-12 col-md-4">موضوع:
                                    <b>{{ $form->subject }}</b>
                                </div>
                                <div class="col-sm-12 col-md-8">متن پیام:
                                    <b>{{ $form->message }}</b>
                                </div>
                                <hr style="height:1px; width:100%; border-width:0; color:lightgray; background-color:lightgray">
                                <div class="col-sm-12 text-right">
                                    <a href="{{ route('admin.form.index') }}" class="btn btn-danger">بازگشت</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Form > a').addClass('active');
    </script>
@endsection
