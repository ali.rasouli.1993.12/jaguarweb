@extends('Admin.layout')
@section('title')
    فرم ها
@endsection
@section('page_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">فرم ها</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
            <li class="breadcrumb-item active">فرم ها</li>
        </ol>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection
@section('main_content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(session('error'))
                    <div class="alert alert-danger alert-dismissible col-sm-3">
                        {{ Session::get('error') }}
                    </div>
                @endif
                @if(session('message'))
                    <div class="alert alert-success alert-dismissible col-sm-3">
                        {{ Session::get('message') }}
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger col-sm-3">
                        {{ $error }}
                    </div>
                @endforeach
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title m-0" style="float: right; padding: 6px 12px;">همه ی فرم ها</h3>
{{--                        <a href="{{ route('admin.client.create') }}" class="btn btn-primary" style="float: left">افزودن فرم جدید</a>--}}
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            @if($forms->count() > 0)
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام شخص / نام شرکت</th>
                                    <th>موبایل</th>
                                    <th>ایمیل</th>
                                    <th>وضعیت</th>
                                    <th style="width: 200px">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($forms as $form)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $form->name }}</td>
                                        <td>{{ $form->mobile }}</td>
                                        <td>{{ $form->email }}</td>
                                        <td>{!! $form->returnLabelStatus($form->visited) !!}</td>
                                        <td class="" style="width: 200px">
                                            <a href="{{ route('admin.form.show', $form->id) }}" class="btn-info btn-sm ml-2">نمایش</a>
                                            <a href="{{ route('admin.form.delete', $form->id) }}" class="btn-danger btn-sm">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            <p class="alert alert-danger p-2">هیچ فرم ای در سایت موجود نیست</p>
                            @endif
                            {{-- {{  $forms->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Form > a').addClass('active');
    </script>
@endsection
