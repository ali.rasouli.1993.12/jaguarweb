@extends('Admin.layout')

@section('title')بلاگ@endsection()

@section('custom_styles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('Admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('page_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">بلاگ</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item active">بلاگ</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible col-sm-3">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" style="float: right">همه ی بلاگ ها</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{ route('admin.blog.insert') }}" method="post" style="width: 100%;" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label" for="title">نام پست</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="title" id="title" placeholder="نام پست">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label" for="title">اسلاگ</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="slug" id="slug" placeholder="اسلاگ">
                                                        <span class="text-danger">بیشتر از 300 کاراکتر نشه</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    {{--                                                <label class="col-sm-2 control-label" for="body">اسلاگ</label>--}}
                                                    <div class="col-sm-12">
                                                        <textarea class="form-control" name="body" id="body"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label" for="shortDescription">توضیح کوتاه</label>
                                                    <div class="col-sm-12">
                                                        <textarea maxlength="255" class="form-control" name="shortDescription" id="shortDescription"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="status" name="status">
                                                    <label class="form-check-label" for="status">وضعیت انتشار</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="imageURL">انتخاب عکس</label>
                                                    <input type="file" class="form-control" name="imageURL" id="imageURL" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="categoryId">انتخاب دسته بندی</label>
                                                    <select class="select2" name="categoryId" id="categoryId"
                                                            data-placeholder="برای انتخاب دسته بندی، تایپ کنید" style="width: 100%;">
                                                        <option selected="selected" value="0">دسته بندی نشده</option>
                                                        @foreach($allCats as $cat)
                                                            <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="tagsId">انتخاب برچسب</label>
                                                    <select class="select2" name="tagsId[]" id="tagsId" multiple="multiple"
                                                            data-placeholder="برای انتخاب برچسب، تایپ کنید" style="width: 100%;">
                                                        @foreach($allTags as $tag)
                                                            <option value="{{ $tag->id }}">{{ $tag->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-sm">ذخیره</button>
                                        <a href="{{ route('admin.blog.index') }}" class="btn btn-danger btn-sm">انصراف</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Blog > a').addClass('active');
    </script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script src="{{ asset('Admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        // $(document).ready(function () {
        $(function () {
            // $('#body').ckeditor();
            $('.select2').select2({
                theme: 'bootstrap4'
            });
        });
    </script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor 4
        // instance, using default configuration.
        CKEDITOR.replace( 'body', {
            language: 'fa',
            // image: 'false'
            filebrowserUploadUrl: '{{ route('posts.upload', ['_token' => csrf_token()]) }}',
            filebrowserUploadMethod: 'form'
        } );
    </script>
@endsection
