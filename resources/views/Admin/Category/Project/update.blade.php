@extends('Admin.layout')
@section('title')بروزرسانی دسته بندی@endsection
@section('page_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">دسته بندی ها</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.tag.project.list') }}">لیست دسته بندی ها</a></li>
                <li class="breadcrumb-item active">بروزرسانی دسته بندی</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible col-sm-3">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title m-0" style="float: right; padding: 6px 12px;">بروزرسانی دسته بندی</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form class="form-horizontal row" action="{{ route('admin.category.project.update', $cat->id) }}" method="post">
                                        @csrf
                                        <div class="col-sm-4">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label" for="title">نام دسته بندی</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="title" id="title"
                                                           placeholder="نام دسته بندی" value="{{ $cat->title }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label" for="slug">اسلاگ دسته بندی</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" disabled name="slug" id="slug" placeholder="اسلاگ دسته بندی">
                                                    <span class="text-danger"> بیشتر از 200 کاراکتر نشود</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label" for="parentId">دسته بندی پدر</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="parentId" id="parentId"
                                                            data-placeholder="انتخاب دسته بندی پدر" style="width: 100%;" >
                                                        <option selected="selected" value="0">انتخاب به عنوان دسته بندی پدر</option>
                                                        @foreach($parentCats as $cat)
                                                            <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group row">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="form-check">
                                                        <input type="checkbox" {{ $cat->status == 1 ? 'checked="checked"' : '' }}
                                                               class="form-check-input" id="status" name="status">
                                                        <label class="form-check-label" for="status">وضعیت انتشار</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary" type="submit">ویرایش</button>
                                            <a href="{{ route('admin.category.project.list') }}" class="btn btn-danger mr-2">انصراف</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Category > a').addClass('active');
    </script>
@endsection
