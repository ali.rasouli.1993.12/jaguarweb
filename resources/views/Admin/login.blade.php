@extends('Admin.loginRegister')

@section('title')
ورود
@endsection

@section('content')
    <div class="login-box">

        @if (isset($errors) && $errors->any())
            <div class="alert alert-danger alert-dismiss col-sm-12">
                <ul style="list-style: none; text-align: right">
                    @foreach ($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('error'))
            <div class="alert text-right alert-danger p-2 alert-dismissible col-sm-12" style="direction: rtl">
                {{ Session::get('error') }}
            </div>
        @endif
        @if(session('message'))
            <div class="alert text-right alert-danger p-2 alert-dismissible col-sm-12" style="direction: rtl">
                {{ Session::get('message') }}
            </div>
        @endif

        <div class="login-logo">
            <a href="{{ route('home') }}"><b>Panel</b> Jaguarweb</a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <!-- /.form-box -->
                <p class="mb-0">
                    <a href="{{ route('showRegister') }}" class="text-center">Register a new user</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
@endsection
