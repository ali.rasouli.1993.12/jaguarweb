@extends('Admin.layout')
@section('title')
    مشتری ها
@endsection
@section('page_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">مشتری ها</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
            <li class="breadcrumb-item active">مشتری ها</li>
        </ol>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection
@section('main_content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(session('error'))
                    <div class="alert alert-danger alert-dismissible col-sm-3">
                        {{ Session::get('error') }}
                    </div>
                @endif
                @if(session('message'))
                    <div class="alert alert-success alert-dismissible col-sm-3">
                        {{ Session::get('message') }}
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger col-sm-3">
                        {{ $error }}
                    </div>
                @endforeach
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title m-0" style="float: right; padding: 6px 12px;">همه ی مشتری ها</h3>
                        <a href="{{ route('admin.client.create') }}" class="btn btn-primary" style="float: left">افزودن مشتری جدید</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            @if($allClients->count() > 0)
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام مشتری / نام شرکت</th>
                                    <th>وضعیت</th>
                                    <th style="width: 200px">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allClients as $client)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $client->title }}</td>
                                        <td>{!! $client->returnLabelStatus($client->status) !!}</td>
                                        <td class="" style="width: 200px">
                                            <a href="{{ route('admin.client.edit', $client->id) }}" class="btn-info btn-sm ml-2">ویرایش</a>
                                            <a href="{{ route('admin.client.delete', $client->id) }}" class="btn-danger btn-sm">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            <p class="alert alert-danger p-2">هیچ مشتری ای در سایت موجود نیست</p>
                            @endif
                            {{-- {{  $allClients->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Client > a').addClass('active');
    </script>
@endsection
