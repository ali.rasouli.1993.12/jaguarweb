@extends('Admin.layout')

@section('title')مشتری@endsection()

@section('page_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">مشتری</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
                <li class="breadcrumb-item active">مشتری</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection

@section('main_content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible col-sm-3">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" style="float: right">افزودن مشتری جدید</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{ route('admin.client.insert') }}" method="post" style="width: 100%;">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label" for="title">نام مشتری</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="title" id="title" placeholder="نام مشتری">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="status" name="status">
                                                    <label class="form-check-label" for="status">وضعیت انتشار</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-sm">ذخیره</button>
                                        <a href="{{ route('admin.client.index') }}" class="btn btn-danger btn-sm">انصراف</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')
    <script>
        $('.nav-item').removeClass('active');
        $('#Client > a').addClass('active');
    </script>
@endsection
