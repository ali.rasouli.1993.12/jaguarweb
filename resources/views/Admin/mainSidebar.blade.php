<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" target="_blank" class="brand-link">
        <img src="{{ asset('Admin/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">صفحه اول</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('Admin/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ auth()->user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item" id="Dashboard">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            داشبورد
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="Blog">
                    <a href="{{ route('admin.blog.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>
                            بلاگ
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="Project">
                    <a href="{{ route('admin.project.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-archive"></i>
                        <p>
                            پروژه ها
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="Category">
                    <a href="{{ route('admin.category.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            دسته بندی ها
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="Tag-Blog">
                    <a href="{{ route('admin.tag.blog.list') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            برچسب های بلاگ
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="Tag-Project">
                    <a href="{{ route('admin.tag.project.list') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            برچسب های پروژه ها
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="Client">
                    <a href="{{  route('admin.client.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            مشتری ها
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="Form">
                    <a href="{{  route('admin.form.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-comment-alt"></i>
                        <p>
                            فرم تماس
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            تنظیمات
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout', auth()->user()->id) }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            خروج
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
