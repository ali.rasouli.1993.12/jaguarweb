<div class="breadcrumbs">
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            @if(request()->route()->getName() == 'showCategory')
                <h2>دسته بندی: {{ $input }}</h2>
            @else
                <h2>{{ $input }}</h2>
            @endif

            <ol class="shadow-lg bg-white">
                <li><i class="bi bi-house-door" style="margin-left: 4px;"></i><a href="{{ route('home') }}">خانه</a></li>
                @if(request()->route()->getName() == 'blog')
                    <li>{{ $input }}</li>
                @elseif(request()->route()->getName() == 'singleBlog')
                    <li><a href="{{ route('blog') }}">بلاگ</a></li>
                    <li>{{ $input }}</li>
                @elseif(request()->route()->getName() == 'showCategory' || request()->route()->getName() == 'portfolio')
                    <li>{{ $input }}</li>
                @endif
            </ol>
        </div>
    </div>
</div>


