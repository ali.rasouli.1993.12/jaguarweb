<!-- ======= Meta Tags ======= -->
    <meta name="robots" content="index, follow">
@if($metadata == 'home')
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:locale" content="fa_IR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="طراحی سایت و سئو  جگوار وب | جگوار" />
    <meta property="og:image" content="{{ asset('Site/assets/img/apple-touch-icon.png') }}" />
    <meta content="طراحی سایت اختصاصی با بهترین قیمت طراحی سایت; طراحی وب سایت جگوار وب ارائه دهنده خدمات طراحی سایت شرکتی، فروشگاهی، شخصی، سئوی حرفه ای و امنیت کامل وب سایت..." name="description">
    <meta content="جگوار وب, طراحی سایت, طراحی وب, سئو, سئو ارزان, امنیت وب سایت, وردپرس, بهینه سازی سایت, سئوی سایت" name="keywords">
    <!-- ======= End Meta Tags ======= -->
@elseif($metadata == 'blog')
    <meta property="og:url" content="{{ route('singleBlog', $blog->slug) }}" />
    <meta property="og:locale" content="fa_IR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $blog->title }}" />
    <meta property="og:image" content="{{ url($blog->imageURL) }}" />
    <meta content="{{ $blog->shortDescription }}" name="description">
    <meta content="@foreach($tags as $tag){{ $tag->title . ',' }}@endforeach" name="keywords">
    <link rel="canonical" href="{{ route('singleBlog', $blog->slug) }}" />
    <!-- ======= End Meta Tags ======= -->
@elseif($metadata == 'project')
    <meta property="og:url" content="{{ route('singlePortfolio', $project->slug) }}" />
    <meta property="og:locale" content="fa_IR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $project->title }}" />
    <meta property="og:image" content="{{ url($project->imageURL) }}" />
    <meta content="{{ $project->shortDescription }}" name="description">
    <meta content="@foreach($tags as $tag){{ $tag->title . ',' }}@endforeach" name="keywords">
    <link rel="canonical" href="{{ route('singlePortfolio', $project->slug) }}" />
    <!-- ======= End Meta Tags ======= -->
@endif
