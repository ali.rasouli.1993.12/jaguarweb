@extends('layout')

@section('title'){{ $blog->title }} - بلاگ@endsection

@section('meta')
    @include('metaTags', ['metadata' => 'blog' ,'blog' => $blog, 'tags' => $tags])
@stop

@section('content')
    <!-- ======= Breadcrumbs ======= -->
    @include('breadcrumb', ['input' => $blog->title])
    <!-- End Breadcrumbs -->

    <!-- ======= Blog Details Section ======= -->
    <section id="blog" class="blog" itemscope itemtype ="http://schema.org/Blog">
        <div class="container" data-aos="fade-up">

            <div class="row g-5">

                <div class="col-lg-8">

                    <article class="blog-details">

                        <div class="post-img">
                            <img src="{{ url($blog->imageURL) }}" alt="{{ $blog->title }}" class="img-fluid">
                        </div>

                        <h2 class="title" itemprop="name">{{ $blog->title }}</h2>

                        <div class="meta-top">
                            <ul>
                                <li class="d-flex align-items-center" itemprop="director"><i class="bi bi-person"></i>مدیر سایت</li>
                                <li class="d-flex align-items-center"><i class="bi bi-clock"></i>
                                    <time datetime="{{ \Illuminate\Support\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d-m-Y') }}">
                                        {{ \Morilog\Jalali\Jalalian::forge($blog->created_at)->format('%d %B %Y') }}
                                    </time>
                                </li>
{{--                                <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="blog-details.html">12 Comments</a></li>--}}
                            </ul>
                        </div><!-- End meta top -->

                        <div class="content">
                            {!! htmlspecialchars_decode($blog->body) !!}
                        </div><!-- End post content -->

                        <div class="meta-bottom">
                            <i class="bi bi-folder"></i>
                            <ul class="cats">
                                <li ><a href="#" itemprop="genre">{{ $blog->category->title ?? '' }}</a></li>
                            </ul>

                            <i class="bi bi-tags"></i>
                            <ul class="tags">
{{--                                {{ unserialize($blog->tagsId) }}--}}
                                @foreach($tags as $tag)
                                <li><a href="#">{{ $tag->title }}</a></li>
                                @endforeach
                            </ul>
                        </div><!-- End meta bottom -->

                    </article><!-- End blog post -->

                </div>

                <div class="col-lg-4">
                    @include('blogSidebar')
                </div>
            </div>

        </div>
    </section><!-- End Blog Details Section -->

@endsection
