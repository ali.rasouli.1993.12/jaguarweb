<!DOCTYPE html>
<html lang="fa-IR" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link rel="alternate" href="{{ request()->url() }}" hreflang="fa-IR" />
    <title>@yield('title', 'طراحی سایت و سئو  جگوار وب') | جگوار وب</title>
    @yield('meta')
    <!-- Favicons -->
    <link href="{{ asset('Site/assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('Site/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
    <!-- Vendor CSS Files -->
    <link href="{{ asset('Site/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('Site/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('Site/assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('Site/assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('Site/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <!-- Variables CSS Files. Uncomment your preferred color scheme -->
    <link href="{{ asset('Site/assets/css/variables.css') }}" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="{{ asset('Site/assets/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('Site/assets/css/IranSansFont.css') }}" rel="stylesheet">
</head>
<body>
<!-- ======= Header ======= -->
@include('header')
@yield('heroSection')
<main id="main">
    @yield('content')
</main><!-- End #main -->
@include('footer')
<div id="preloader"></div>
<!-- Vendor JS Files -->
<script src="{{ asset('Site/assets/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('Site/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('Site/assets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('Site/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('Site/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('Site/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<!-- Template Main JS File -->
<script src="{{ asset('Site/assets/js/main.js') }}"></script>
@yield('footer-script')
</body>
</html>
