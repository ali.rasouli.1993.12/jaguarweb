<section id="hero-animated" class="hero-animated d-flex align-items-center">
    <div class="container d-flex flex-column justify-content-center align-items-center text-center position-relative" data-aos="zoom-out">
        <img src="{{ asset('Site/assets/img/hero-carousel/hero-carousel-3.svg') }}" class="img-fluid animated" alt="جگوار وب | طراحی سایت">
        <h2>به <span>جگوار وب</span> خوش آمدید</h2>
        <p>کسب و کار خود را با جگوار‌وب توسعه دهید ما تیمی از طراحان با استعداد هستیم که با بوت استرپ وب سایت می سازیم</p>
        <div class="d-flex">
            <a href="#about" class="btn-get-started scrollto">شروع کنید</a>
            <a href="{{ asset('Site/assets/video/video.mp4') }}" class="glightbox btn-watch-video d-flex align-items-center"><span>نمایش ویدئو</span><i class="bi bi-play-circle"></i></a>
        </div>
    </div>
</section>
