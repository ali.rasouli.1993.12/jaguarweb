@extends('layout')

@section('heroSection')@include('hero')@stop

@section('meta')
    @include('metaTags', ['metadata' => 'home'])
@stop

@section('content')
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
        <div class="container">

            <div class="row gy-4">

                <div class="col-xl-3 col-md-6 d-flex" data-aos="zoom-out">
                    <div class="service-item position-relative">
                        <div class="icon"><i class="bi bi-activity icon"></i></div>
                        <h4><a href="https://jaguarweb.ir/blog/website-page-load-speed" class="stretched-link">افزایش سرعت سایت</a></h4>
                        <p>افزایش سرعت وب سایت تنها در 7 روز</p>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-3 col-md-6 d-flex" data-aos="zoom-out" data-aos-delay="200">
                    <div class="service-item position-relative">
                        <div class="icon"><i class="bi bi-bounding-box-circles icon"></i></div>
                        <h4><a href="https://jaguarweb.ir/blog/hacked-website-repair" class="stretched-link">پاکسازی بدافزار</a></h4>
                        <p>امنیت سایت و رفع مشکل هک شدن سایت شما</p>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-3 col-md-6 d-flex" data-aos="zoom-out" data-aos-delay="400">
                    <div class="service-item position-relative">
                        <div class="icon"><i class="bi bi-calendar4-week icon"></i></div>
                        <h4><a href="https://jaguarweb.ir/blog/website-bugs" class="stretched-link">رفع ایراد</a></h4>
                        <p>رفع ایرادات کلی و جزئی وب سایت شما</p>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-3 col-md-6 d-flex" data-aos="zoom-out" data-aos-delay="600">
                    <div class="service-item position-relative">
                        <div class="icon"><i class="bi bi-broadcast icon"></i></div>
                        <h4><a href="https://jaguarweb.ir/blog/search-engine-optimization-seo" class="stretched-link">سئو - SEO</a></h4>
                        <p>سئوی کلاه سفید در 3 ماه با ماندگاری طولانی</p>
                    </div>
                </div><!-- End Service Item -->
            </div>

        </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>درباره ما</h2>
                <p>طراحی انواع سایتهای شرکتی، فروشگاهی، شخصی و ... با آخرین متدهای برنامه نویسی و با سیستم مدیریت محتوای اختصاصی یا وردپرس با رعایت اصول سئو و امنیت کامل.</p>
            </div>

            <div class="row g-4 g-lg-5" data-aos="fade-up" data-aos-delay="200">

                <div class="col-lg-5">
                    <div class="about-img">
                        <img src="{{ asset('Site/assets/img/about.jpg') }}" class="img-fluid" alt="درباره ما">
                    </div>
                </div>

                <div class="col-lg-7">
                    <h3 class="pt-0 pt-lg-5">طراحی و ساخت وب سایت با هر سلیقه‌ای با پشتیبانی رایگان 3 ماهه همراه با امنیت بالا و سئوی اولیه مناسب و کدنویسی استاندارد در جگوار وب.</h3>

                    <!-- Tabs -->
                    <ul class="nav nav-pills mb-3">
                        <li><a class="nav-link active" data-bs-toggle="pill" href="#tab1">طراحی</a></li>
                        <li><a class="nav-link" data-bs-toggle="pill" href="#tab2">سئو</a></li>
                        <li><a class="nav-link" data-bs-toggle="pill" href="#tab3">پشتیبانی</a></li>
                    </ul><!-- End Tabs -->

                    <!-- Tab Content -->
                    <div class="tab-content">

                        <div class="tab-pane fade show active" id="tab1">

                            <p class="fst-italic">طراحی و کد نویسی استاندارد</p>

                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>یکی از مشکلات بزرگ استفاده از قالب های آماده برای وب سایت، این است که طراحی آن ها ارتباط بسیار کمی با برند شما دارد (اگر اصلا ارتباطی داشته باشد). به عبارت دیگر، شاید بتوانید موارد کوچکی مانند فونت نوشته-ها و ترکیب رنگی سایت را تغییر دهید، اما هیچ چیزی به صورت اختصاصی برای برند خود نخواهید داشت.</h4>
                            </div>
                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>ساختار و معماری سایت شما باید شهودی، تطابق پذیر، و تمیز و مرتب باشد. یعنی تعداد کدهای به کار رفته باید به حداقل برسد و صرفا آن چه نیاز دارید را پوشش دهد، تا سایتی با زیرساختی قوی و مرتب داشته باشید که به سرعت بارگذاری می شود.</h4>
                            </div>
                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>استفاده از تصاویر آماده برای طراحی وب سایت کاری ساده و وسوسه انگیز به نظر می رسد، اما اگر می خواهید برای داشتن سایتی درجه یک هزینه کنید باید پولی را هم برای عکاسی اختصاصی حرفه ای کنار بگذارید.</h4>
                            </div>
                        </div><!-- End Tab 1 Content -->

                        <div class="tab-pane fade show" id="tab2">

                            <p class="fst-italic">سئو، بهینه سازی و امنیت اولیه رایگان</p>

                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>موتورهای جستجو به مانند ماشین های پاسخگویی هستند. آنها در بین میلیارد ها محتوا به جستجو پرداخته و با ارزیابی هزاران فاکتور تشخیص می هند که کدام محتوا بیشترین نزدیکی را به پاسخ سوال شما دارد.</h4>
                            </div>

                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>سئو یکی از تنها روش های بازاریابیست که در صورت پیاده سازی صحیح، تاثیر آن در طول زمان افزایش نیز می یابد. در صورتی که شما دارای محتوایی در وب سایت باشید که مستحق کسب رتبه باشد، ترافیک کسب شده از این محتوا در طول زمان افزایش نیز پیدا خواهد کرد، در حالی که تبلیغات برای اعمال شدن نیاز به صرف هزینه و شارژ کردن حساب دارد.</h4>
                            </div>

                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>سئو داخلی بر روی فاکتورهایی از رتبه بندی گوگل متمرکز است که مستقیما به وضعیت صفحه مورد نظر برای بهینه سازی وابسته است. مانند هدینگ ها، محتوا و ساختار صفحه.</h4>
                            </div>

                        </div><!-- End Tab 2 Content -->

                        <div class="tab-pane fade show" id="tab3">

                            <p class="fst-italic">پشتیبانی فنی رایگان 3 ماهه</p>

                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>مزایای پشتیبانی سایت</h4>
                            </div>
                            <p>وب سایت شما ابزاری قدرتمند برای بازاریابی است .یک وب سایت خوب و مناسب می تواند به شما در دستیابی به مزیت رقابتی در صنعت و بهبود چهره کسب و کار شما کمک کند.</p>

                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>دسترسی</h4>
                            </div>
                            <p>مشتری ها تمام روز فقط در خانه یا دفتر کار نمی نشینند . آنها مشغول زندگی در سراسر جهانی هستند که همواره آنها را آنها ، با مشکلاتی روبرو می کند .اگر یک محصول خراب شود بلافاصله یک راه حل می خواهند.</p>

                            <div class="d-flex align-items-center mt-4">
                                <i class="bi bi-check2"></i>
                                <h4>استفاده آسان و به روز رسانی</h4>
                            </div>
                            <p>اگر به درستی نگهداری شود ، وب سایت شما همیشه به روز و جاری خواهد بود. به راحتی بروزرسانی ها ، ویرایش ها و حذف ها از هر رایانه ای در اینترنت را انجام دهید. دیگر لازم نیست هر بار که می خواهید این کار را کنید یا یک محصول اضافه کنید ، به یک برنامه نویس مراجعه کنید.</p>

                        </div><!-- End Tab 3 Content -->

                    </div>

                </div>

            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
        <div class="section-header">
            <h2>تکنولوژی ها</h2>
            <p>تکنولوژی های مورد استفاده ما را در زیر مشاهده می کنید</p>
        </div>
        <div class="container" data-aos="zoom-out">

            <div class="clients-slider swiper">
                <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-1.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-2.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-3.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-4.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-5.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-6.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-7.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-8.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-9.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                    <div class="swiper-slide"><img src="{{ asset('Site/assets/img/clients/client-10.png') }}" class="img-fluid" alt="تکنولوژی های مورد استفاده"></div>
                </div>
            </div>

        </div>
    </section><!-- End Clients Section -->

    <!-- ======= Call To Action Section ======= -->
    <section id="cta" class="cta">
        <div class="container" data-aos="zoom-out">


            <div class="row g-5">

                <div class="col-lg-8 col-md-6 content d-flex flex-column justify-content-center order-last order-md-first">
                    <h3>پشتیبانی 24/4<em>حتی روزهای تعطیل</em>بصورت کامل و سریع</h3>
                    <p>پشتیبانی فنی و محتوایی وب سایت شما و برطرف شدن مشکل در کمترین زمان ممکن با جگوار وب :)</p>
                    <a class="cta-btn align-self-start scrollto" href="#contact">تماس سریع</a>
                </div>

                <div class="col-lg-4 col-md-6 order-first order-md-last d-flex align-items-center">
                    <div class="img">
                        <img src="{{ asset('Site/assets/img/cta.jpg') }}" alt="پشتیبانی" class="img-fluid">
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Call To Action Section -->

    <!-- ======= Features Section ======= -->
    <section id="features" class="features">
        <div class="container">
            <div class="section-header aos-init aos-animate" data-aos="fade-up">
                <h2>ویژگی ها</h2>
                <p>قابلیت هایی که می توان به پروژه ها افزود.</p>
            </div>
            <div class="row aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
                <div class="col-lg-3 col-md-4">
                    <div class="icon-box">
                        <i class="trait life bi bi-alarm-fill"></i>
                        <h3><a href="#!">نمایش عمر پروژه ها</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="trait visit bi bi-bar-chart-line"></i>
                        <h3><a href="#!">آمار بازدیدکننده ها</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="trait time bi bi-calendar-date"></i>
                        <h3><a href="#!">نمایش مطلب در تاریخ معین</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4 mt-lg-0">
                    <div class="icon-box">
                        <i class="trait color bi bi-palette"></i>
                        <h3><a href="#!">تغییر رنگ بندی</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="trait host bi bi-arrow-bar-up"></i>
                        <h3><a href="#!">افزایش میزان هاست</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="trait bandwidth bi bi-kanban"></i>
                        <h3><a href="#!">مدیریت پهنای باند</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="bi content2 bi-paragraph"></i>
                        <h3><a href="#!">تولید محتوای  اختصاصی</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="trait registeruser bi bi-person-bounding-box"></i>
                        <h3><a href="#!">عضویت کاربر</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="trait security bi bi-shield-check"></i>
                        <h3><a href="#!">امنیت کامل</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="trait media bi bi-file-earmark-music"></i>
                        <h3><a href="#!">قراردادن انواع رسانه</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="trait access bi bi-hand-thumbs-up"></i>
                        <h3><a href="#!">دسترس 24/7</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="trait passmanager bi bi-fingerprint"></i>
                        <h3><a href="#!">مدیریت پسورد</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Features Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>خدمات ما</h2>
                <p>برخی از خدمات و کارهایی که جگوار وب انجام می دهد را مشاهده می کنید.</p>
            </div>

            <div class="row gy-5">

                <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="200">
                    <div class="service-item">
                        <div class="img">
                            <img src="{{ asset('Site/assets/img/services-1.jpg') }}" class="img-fluid" alt="خدمات">
                        </div>
                        <div class="details position-relative">
                            <div class="icon">
                                <i class="bi bi-activity"></i>
                            </div>
                            <a href="#!" class="stretched-link">
                                <h3>طراحی سایت</h3>
                            </a>
                            <p>طراحی انواع سایتهای شرکتی، فروشگاهی، پنل‌های قدرتمند اختصاصی با آخرین متدهای برنامه نویسی و با سیستم مدیریت محتوای اختصاصی یا وردپرس با رعایت اصول سئو.</p>
                        </div>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="300">
                    <div class="service-item">
                        <div class="img">
                            <img src="{{ asset('Site/assets/img/services-2.jpg') }}" class="img-fluid" alt="خدمات ما">
                        </div>
                        <div class="details position-relative">
                            <div class="icon">
                                <i class="bi bi-broadcast"></i>
                            </div>
                            <a href="#!" class="stretched-link">
                                <h3>سئو</h3>
                            </a>
                            <p>پیاده سازی سایت بدون بهینه سازی و سئو، برابر است با دیده نشدن و اتلاف وقت! جگوار وب آخرین فاکتورها و قواعد گوگل را برای بالا بردن ورودی ارگانیک پروژه‌ها بکار می گیرد.</p>
                        </div>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="400">
                    <div class="service-item">
                        <div class="img">
                            <img src="{{ asset('Site/assets/img/services-3.jpg') }}" class="img-fluid" alt="خدمات ما">
                        </div>
                        <div class="details position-relative">
                            <div class="icon">
                                <i class="bi bi-easel"></i>
                            </div>
                            <a href="#!" class="stretched-link">
                                <h3>سفارش تولید محتوا</h3>
                            </a>
                            <p>محتوا مهمترین بخش سایت شماست، با سفارش تولید محتوا با کیفیت ، اختصاصی و سئو شده آی وحید در انواع مختلف متنی، صوتی، تصویری 90 درصد موفقیت سایت خود را تضمین کرده و تحول در بازاریابی و لذت تفاوت را تجربه کنید.</p>
                        </div>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="500">
                    <div class="service-item">
                        <div class="img">
                            <img src="{{ asset('Site/assets/img/services-4.jpg') }}" class="img-fluid" alt="خدمات ما">
                        </div>
                        <div class="details position-relative">
                            <div class="icon">
                                <i class="bi bi-bounding-box-circles"></i>
                            </div>
                            <a href="#!" class="stretched-link">
                                <h3>طراحی اپلیکیشن</h3>
                            </a>
                            <p>طراحی انواع اپلیکیشن ها مطابق با تکنولوژی های روز دنیا، تخصص آی وحید است. ما اپلیکیشن اندروید و IOS مورد نیاز شما را به شکلی حرفه ای، کاملا اختصاصی و متفاوت پیاده سازی خواهیم کرد.</p>
                            <a href="#!" class="stretched-link"></a>
                        </div>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="600">
                    <div class="service-item">
                        <div class="img">
                            <img src="{{ asset('Site/assets/img/services-5.jpg') }}" class="img-fluid" alt="خدمات ما">
                        </div>
                        <div class="details position-relative">
                            <div class="icon">
                                <i class="bi bi-calendar4-week"></i>
                            </div>
                            <a href="#!" class="stretched-link">
                                <h3>امنیت سایت</h3>
                            </a>
                            <p>امنیت سایت به مجموعه اقداماتی گفته می شود که با انجام آنها ضریب امنیت سایت به حداکثر رسیده و امکان نفوذ به آن به حداقل می رسد. توجه داشته باشید که امنیت سایت تابع موارد دیگری نیز می باشد که تمامی آنها نقش بسیار اساسی در تامین امنیت وبسایت دارند.</p>
                            <a href="#!" class="stretched-link"></a>
                        </div>
                    </div>
                </div><!-- End Service Item -->

                <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="700">
                    <div class="service-item">
                        <div class="img">
                            <img src="{{ asset('Site/assets/img/services-6.jpg') }}" class="img-fluid" alt="خدمات ما">
                        </div>
                        <div class="details position-relative">
                            <div class="icon">
                                <i class="bi bi-chat-square-text"></i>
                            </div>
                            <a href="#!" class="stretched-link">
                                <h3>طراحی گرافیک سایت</h3>
                            </a>
                            <p>تمرکز بر روی طراحی گرافیک وب این است که چگونه هنرمندی می‌تواند تجربیاتی را ایجاد کند که فقط ویژگی نمایشی نداشته باشد بلکه مردم بتوانند با آن‌ تعامل نیز داشته باشند.</p>
                            <a href="#!" class="stretched-link"></a>
                        </div>
                    </div>
                </div><!-- End Service Item -->

            </div>

        </div>
    </section><!-- End Services Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials d-none">
        <div class="container" data-aos="fade-up">

            <div class="testimonials-slider swiper">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('Site/assets/img/testimonials/testimonials-1.jpg') }}" class="testimonial-img" alt="مشتریان ما">
                            <h3>Saul Goodman</h3>
                            <h4>Ceo &amp; Founder</h4>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                <i class="bi bi-quote quote-icon-left"></i>
                                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                                <i class="bi bi-quote quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('Site/assets/img/testimonials/testimonials-2.jpg') }}" class="testimonial-img" alt="مشتریان ما">
                            <h3>Sara Wilsson</h3>
                            <h4>Designer</h4>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                <i class="bi bi-quote quote-icon-left"></i>
                                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                                <i class="bi bi-quote quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('Site/assets/img/testimonials/testimonials-3.jpg') }}" class="testimonial-img" alt="مشتریان ما">
                            <h3>Jena Karlis</h3>
                            <h4>Store Owner</h4>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                <i class="bi bi-quote quote-icon-left"></i>
                                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                                <i class="bi bi-quote quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('Site/assets/img/testimonials/testimonials-4.jpg') }}" class="testimonial-img" alt="مشتریان ما">
                            <h3>Matt Brandon</h3>
                            <h4>Freelancer</h4>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                <i class="bi bi-quote quote-icon-left"></i>
                                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                                <i class="bi bi-quote quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('Site/assets/img/testimonials/testimonials-5.jpg') }}" class="testimonial-img" alt="مشتریان ما">
                            <h3>John Larson</h3>
                            <h4>Entrepreneur</h4>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                <i class="bi bi-quote quote-icon-left"></i>
                                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                                <i class="bi bi-quote quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>تعرفه های ما</h2>
                <p>قیمت ها با توجه با درخواست نهایی تغییر خواهد کرد.</p>
            </div>

            <div class="row gy-4">

                <div class="col-lg-4" data-aos="zoom-in" data-aos-delay="200">
                    <div class="pricing-item">

                        <div class="pricing-header">
                            <h3>اقتصادی</h3>
                            <h4><span>20 میلیون تومان</span></h4>
                        </div>

                        <ul>
                            <li><i class="bi bi-dot"></i> <span>سایت وردپرس</span></li>
                            <li><i class="bi bi-dot"></i> <span>قالب تجاری (آماده)</span></li>
                            <li><i class="bi bi-dot"></i> <span>پشتیبانی 3 ماهه</span></li>
                            <li class="na"><i class="bi bi-x"></i> <span>امنیت</span></li>
                            <li class="na"><i class="bi bi-x"></i> <span>سئو</span></li>
                            <li class="na"><i class="bi bi-x"></i> <span>شبکه های مجازی</span></li>
                        </ul>

                        <div class="text-center mt-auto">
                            <a href="{{ route('home') }}#contact" class="buy-btn scrollto">سفارش دهید</a>
                        </div>

                    </div>
                </div><!-- End Pricing Item -->

                <div class="col-lg-4" data-aos="zoom-in" data-aos-delay="400">
                    <div class="pricing-item featured">

                        <div class="pricing-header">
                            <h3>پیشنهادی</h3>
                            <h4><span>30 میلیون تومان</span></h4>
                        </div>

                        <ul>
                            <li><i class="bi bi-dot"></i> <span>سایت وردپرس</span></li>
                            <li><i class="bi bi-dot"></i> <span>لاراول آماده</span></li>
                            <li><i class="bi bi-dot"></i> <span>قالب آماده</span></li>
                            <li><i class="bi bi-dot"></i> <span>پشتیبانی 3 ماهه</span></li>
                            <li><i class="bi bi-dot"></i> <span>امنیت</span></li>
                            <li class="na"><i class="bi bi-x"></i> <span>سئو</span></li>
                            <li class="na"><i class="bi bi-x"></i> <span>شبکه های مجازی</span></li>
                        </ul>

                        <div class="text-center mt-auto">
                            <a href="{{ route('home') }}#contact" class="buy-btn scrollto">سفارش دهید</a>
                        </div>

                    </div>
                </div><!-- End Pricing Item -->

                <div class="col-lg-4" data-aos="zoom-in" data-aos-delay="600">
                    <div class="pricing-item">

                        <div class="pricing-header">
                            <h3>اختصاصی</h3>
                            <h4><span>50 میلیون تومان</span></h4>
                        </div>

                        <ul>
                            <li><i class="bi bi-dot"></i> <span>لاراول اختصاصی</span></li>
                            <li><i class="bi bi-dot"></i> <span>ری اکت اختصاصی</span></li>
                            <li><i class="bi bi-dot"></i> <span>قالب اختصاصی</span></li>
                            <li><i class="bi bi-dot"></i> <span>پشتیبانی 9 ماهه</span></li>
                            <li><i class="bi bi-dot"></i> <span>امنیت</span></li>
                            <li><i class="bi bi-dot"></i> <span>سئو</span></li>
                            <li><i class="bi bi-dot"></i> <span>شبکه های مجازی</span></li>
                        </ul>

                        <div class="text-center mt-auto">
                            <a href="{{ route('home') }}#contact" class="buy-btn scrollto">سفارش دهید</a>
                        </div>

                    </div>
                </div><!-- End Pricing Item -->

            </div>

        </div>
    </section><!-- End Pricing Section -->

    <!-- ======= F.A.Q Section ======= -->
    <section id="faq" class="faq">
        <div class="container-fluid aos-init aos-animate" data-aos="fade-up">

            <div class="row gy-4">

                <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                    <div class="content px-xl-5">
                        <h3>پر تکرار ترین <strong>سوالات</strong></h3>
                        <p>سوال هایی که بیشتر از همه در زمینه طراحی سایت، سئو، پشتیبانی و ... از ما پرسیده شده است.</p>
                    </div>

                    <div class="accordion accordion-flush px-xl-5" id="faqlist">

                        <div class="accordion-item aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                            <h3 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1" aria-expanded="false">
                                    <i class="bi bi-question-circle question-icon"></i>سئو کلاً چی هست ؟</button>
                            </h3>
                            <div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist" style="">
                                <div class="accordion-body">به زبان ساده سئو (SEO) یعنی سایت شما در صفحات اول گوگل بصورت کاربری بیاد.</div>
                            </div>
                        </div><!-- # Faq item-->

                        <div class="accordion-item aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
                            <h3 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-2" aria-expanded="false">
                                    <i class="bi bi-question-circle question-icon"></i>طراحی سایت ارزان با گران چقدر تفاوت داره؟</button>
                            </h3>
                            <div id="faq-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist" style="">
                                <div class="accordion-body">تفاوت اصلی در نوع کدنویسی و بهینه و استاندارد بودن هست از طرفی پشتیبانی قوی و سئوی خوب قطعاً مهمترین عامل تعیین کننده میزان قیمت در طراحی و قیمت نهایی سایت می باشد.</div>
                            </div>
                        </div><!-- # Faq item-->

                        <div class="accordion-item aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">
                            <h3 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-3" aria-expanded="false">
                                    <i class="bi bi-question-circle question-icon"></i>امنیت سایت برای چه چیزی هست؟</button>
                            </h3>
                            <div id="faq-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist" style="">
                                <div class="accordion-body">این مسئله که اطلاعات شما و کاربران شما در دسترس شخص خارجی و ثالث قرار نگیرد در حدی مهم هست که برخی شرکت ها میلیون ها دلار صرف امنیت می کنند؛ اما امنیت رو باید قبل از شروع کار مدنظر داشت که کمترین هزینه صرف آن شود و با اطمینان ترین وب سایت رو در اختیار کاربران خودتون قرار بدید.</div>
                            </div>
                        </div><!-- # Faq item-->

                        <div class="accordion-item aos-init aos-animate" data-aos="fade-up" data-aos-delay="500">
                            <h3 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-4" aria-expanded="false">
                                    <i class="bi bi-question-circle question-icon"></i>پشتیبانی فنی چی هست؟</button>
                            </h3>
                            <div id="faq-content-4" class="accordion-collapse collapse" data-bs-parent="#faqlist" style="">
                                <div class="accordion-body">
                                    <i class="bi bi-question-circle question-icon"></i>پشتیبانی فنی به مواردی گفته میشه برای زنده بودن سایت در زمان آنی مهم هست، برای مثال رفع یک خطای کدنویسی و یا خطای سرور و یا بروز رسانی بخش های مختلف سایت همگی جزئی از پشتیبانی فنی سایت محسوب می شود.</div>
                            </div>
                        </div><!-- # Faq item-->

                        <div class="accordion-item aos-init aos-animate" data-aos="fade-up" data-aos-delay="600">
                            <h3 class="accordion-header">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-5" aria-expanded="true">
                                    <i class="bi bi-question-circle question-icon"></i>پشتیبانی محتوایی چی هست؟</button>
                            </h3>
                            <div id="faq-content-5" class="accordion-collapse collapse show" data-bs-parent="#faqlist" style="">
                                <div class="accordion-body">ارطال مطالب در وب سایت با توجه به اصول سئو و بهینه برای کاربران و البته با توجه به موضوع وب سایت و زبان وب سایت (فارسی، عربی، انگلیسی و ...) انتخاب تصاویر مناسب، کلمات کلیدی مناسب و ... که باعث می شود کاربران جذب وب سایت شما شود همگی جزئی از پشتیبانی محتوایی هستند.</div>
                            </div>
                        </div><!-- # Faq item-->

                    </div>

                </div>

                <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style="background-image: url(https://jaguarweb.ir/Site/assets/img/faq.jpg);">&nbsp;</div>
            </div>

        </div>
    </section><!-- End F.A.Q Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio" data-aos="fade-up">

        <div class="container">

            <div class="section-header">
                <h2>نمونه کار ها</h2>
                <p>برخی نمونه کارها</p>
            </div>

        </div>

        <div class="container-fluid" data-aos="fade-up" data-aos-delay="200">

            <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order">

                <ul class="portfolio-flters">
                    <li data-filter="*" class="filter-active">همه</li>
                    @if ($portfolioCats->count() > 0)
                        @foreach ($portfolioCats as $portCat)
                            <li data-filter=".filter-{{ $portCat->slug }}">{{ $portCat->title }}</li>
                        @endforeach
                    @endif
                </ul><!-- End Portfolio Filters -->

                <div class="row g-0 portfolio-container">
                    @foreach ($projects as $project)
                    <div class="col-xl-3 col-lg-4 col-md-6 portfolio-item {{ $project->category->slug ? 'filter-'.$project->category->slug : '' }}">
                        <img src="{{ url($project->imageURL) }}" class="img-fluid" alt="{{ $project->title }}">
                        <div class="portfolio-info">
                            <h4>{{ $project->title }}</h4>
                            <a href="{{ url($project->imageURL) }}" title="$project->title" data-gallery="{{ $project->category->slug ?? '' }}" class="glightbox preview-link">
                                <i class="bi bi-zoom-in"></i>
                            </a>
                            <a href="{{ route('singlePortfolio', $project->slug) }}" title="More Details" class="details-link">
                                <i class="bi bi-link-45deg"></i>
                            </a>
                        </div>
                    </div><!-- End Portfolio Item -->
                    @endforeach
                </div><!-- End Portfolio Container -->
            </div>

        </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>تیم ما</h2>
                <p>اعضای خوب ما</p>
            </div>

            <div class="row gy-5">

                <div class="col-xl-6 col-md-6 d-flex" data-aos="zoom-in" data-aos-delay="200">
                    <div class="team-member">
                        <div class="member-img">
                            <img src="{{ asset('Site/assets/img/team/team-1.jpg') }}" class="img-fluid" alt="علی رسولی">
                        </div>
                        <div class="member-info">
                            <div class="social">
                                <a href="#!"><i class="bi bi-twitter"></i></a>
                                <a href="#!"><i class="bi bi-facebook"></i></a>
                                <a href="#!"><i class="bi bi-instagram"></i></a>
                                <a href="#!"><i class="bi bi-linkedin"></i></a>
                            </div>
                            <h4>علی رسولی</h4>
                            <span>برنامه نویس و مدیر ارشد پروژه</span>
                        </div>
                    </div>
                </div><!-- End Team Member -->

                <div class="col-xl-6 col-md-6 d-flex" data-aos="zoom-in" data-aos-delay="400">
                    <div class="team-member">
                        <div class="member-img">
                            <img src="{{ asset('Site/assets/img/team/team-3.jpg') }}" class="img-fluid" alt="احمدرضا پری پیکر">
                        </div>
                        <div class="member-info">
                            <div class="social">
                                <a href="#!"><i class="bi bi-twitter"></i></a>
                                <a href="#!"><i class="bi bi-facebook"></i></a>
                                <a href="#!"><i class="bi bi-instagram"></i></a>
                                <a href="#!"><i class="bi bi-linkedin"></i></a>
                            </div>
                            <h4>احمدرضا پری پیکر</h4>
                            <span>مدیر عامل و مدیر بخش سئو</span>
                        </div>
                    </div>
                </div><!-- End Team Member -->



            </div>

        </div>
    </section><!-- End Team Section -->

    <!-- ======= Recent Blog Posts Section ======= -->
    <section id="recent-blog-posts" class="recent-blog-posts" data-aos="fade-up">

        <div class="container">

            <div class="section-header">
                <h2>بلاگ</h2>
                <p>آخرین بلاگ های ما</p>
                <a href="{{ route('blog') }}" target="_blank" class="btn btn-light btn-outline-secondary">
                    نمایش همه
                </a>
            </div>

            <div class="row">
                @foreach($blogs as $blog)
                    <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
                        <div class="post-box">
                            <div class="post-img"><img src="{{ url($blog->imageURL) }}" class="img-fluid" alt="{{ $blog->title }}"></div>
                            <div class="meta">
                                <span class="post-author">مدیر سایت</span>
                                <span class="post-date">{{ \Morilog\Jalali\Jalalian::forge($blog->created_at)->format('%d %B %Y') }}</span>
                            </div>
                            <h3 class="post-title">{{ $blog->title }}</h3>
                            <p>{{ \Illuminate\Support\Str::limit($blog->shortDescription, 80) }}</p>
                            <a href="{{ route('singleBlog', $blog->slug) }}" class="readmore stretched-link">
                                <span>ادامه مطلب</span><i class="bi bi-arrow-left"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

    </section><!-- End Recent Blog Posts Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-header">
                <h2>تماس با ما</h2>
                <p>شما می توانید از طریق فرم زیر با ما تماس بگیرید.</p>
            </div>

        </div>

        <div class="container">

            <div class="row gy-5 gx-lg-5">

                <div class="col-lg-4">

                    <div class="info">
                        <h3>فرم تماس</h3>
                        <p>فرم زیر را پر کنید و در منتظر تماس کارشناسان ما باشید.</p>

                        <div class="info-item d-flex">
                            <i class="bi bi-geo-alt flex-shrink-0"></i>
                            <div>
                                <h4>آدرس:</h4>
                                <p>البرز، گوهردشت، اشتراکی شمالی، خیابان گلستان 23 پلاک 42</p>
                            </div>
                        </div><!-- End Info Item -->

                        <div class="info-item d-flex">
                            <i class="bi bi-envelope flex-shrink-0"></i>
                            <div>
                                <h4>ایمیل:</h4>
                                <p><a href="mailto:info@jaguarweb.ir" class="__cf_email__" data-cfemail="066f68606946637e676b766a632865696b">info [at] jaguarweb.ir</a></p>
                            </div>
                        </div><!-- End Info Item -->

                        <div class="info-item d-flex">
                            <i class="bi bi-phone flex-shrink-0"></i>
                            <div>
                                <h4>شماره تماس:</h4>
                                <p><a href="tel:09378248297">0937-824-8297</a></p>
                                <p><a href="tel:09023839935">0902-383-9935</a></p>
                            </div>
                        </div><!-- End Info Item -->

                    </div>

                </div>

                <div class="col-lg-8">
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible col-sm-3">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger col-sm-3">
                            {{ $error }}
                        </div>
                    @endforeach
                    <form action="{{ route('form') }}" method="post" class="php-email-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="نام و نام خانوادگی" required>
                            </div>
                            <div class="col-md-6 form-group mt-3 mt-md-0">
                                <input type="email" class="form-control" name="email" id="email" placeholder="ایمیل" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group mt-3">
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="موضوع" required>
                            </div>
                            <div class="col-md-6 form-group mt-3">
                                <input type="text" class="form-control" name="mobile" id="mobile" placeholder="شماره همراه" required>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" name="message" id="message" placeholder="پیام" required></textarea>
                        </div>
                        <div class="form-group mt-3 mb-4">
                            <input id="captcha" type="text" class="form-control" placeholder="کپتچا را وارد کنید" name="captcha">
                        </div>
                        <div class="form-group">
                            <div class="captcha">
                                {{-- <span>{!! captcha_img() !!}</span> --}}
                                <span><img src="{{ captcha_src() }}" alt="captcha" ></span>
                                <button type="button" class="btn btn-danger reload" id="reload">
                                    &#x21bb;
                                </button>
                            </div>
                        </div>
                        <div class="text-center"><button type="submit">ارسال پیام</button></div>
                    </form>
                </div><!-- End Contact Form -->

            </div>

        </div>
    </section><!-- End Contact Section -->
@endsection

@section('footer-script')
<script>
        $('#reload').click(function () {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function (data) {
                    $(".captcha span img").attr('src', data.captcha);
                }
            });
        });
</script>
@endsection
