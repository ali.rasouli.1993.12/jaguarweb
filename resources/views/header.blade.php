<header id="header" class="header fixed-top" data-scrollto-offset="0">
    <div class="container-fluid d-flex align-items-center justify-content-between">

        <a href="{{ route('home') }}" class="logo d-flex align-items-center scrollto me-auto me-lg-0">
            <h1>جگوار وب<span>.</span></h1>
        </a>

        <nav id="navbar" class="navbar">
            <ul>
                <li class="dropdown"><a href="{{ route('home') }}"><span>خانه</span></a></li>
                <li><a class="nav-link scrollto" href="{{ route('home') }}#about">درباره</a></li>
                <li><a class="nav-link scrollto" href="{{ route('home') }}#services">خدمات</a></li>
                <li><a class="nav-link scrollto" href="{{ route('home') }}#portfolio">نمونه کار</a></li>
                <li><a class="nav-link scrollto" href="{{ route('home') }}#team">اعضا</a></li>
                <li><a class="nav-link scrollto" href="{{ route('home') }}#recent-blog-posts">بلاگ</a></li>
                <li><a class="nav-link scrollto" href="{{ route('home') }}#contact">تماس</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle d-none"></i>
        </nav><!-- .navbar -->

        <a class="btn-getstarted scrollto" href="{{ route('home') }}#about">شروع کنید</a>

    </div>
</header><!-- End Header -->
