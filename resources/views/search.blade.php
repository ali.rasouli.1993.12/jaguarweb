@extends('layout')

@section('content')
    <!-- ======= Breadcrumbs ======= -->
    @include('breadcrumb', ['input' => 'جستجو برای: '. $searchText])
    <!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
        <div class="container" data-aos="fade-up">

            <div class="row g-5">

                <div class="col-lg-8">

                    @if($allBlogs->count() > 0)
                        <div class="row gy-4 posts-list">
                        @foreach ($allBlogs as $blog)
                            <div class="col-lg-6">
                                <article class="d-flex flex-column">

                                    <div class="post-img">
                                        <img src="{{ url($blog->imageURL) }}"
                                             alt="{{ $blog->title }}" class="img-fluid">
                                    </div>

                                    <h2 class="title">
                                        <a href="{{ route('singleBlog', $blog->slug) }}">{{ $blog->title }}</a>
                                    </h2>

                                    <div class="meta-top">
                                        <ul>
                                            <li class="d-flex align-items-center" style="margin-left: 20px;">
                                                <i class="bi bi-person"></i>مدیر سایت
                                            </li>
                                            <li class="d-flex align-items-center">
                                                <i class="bi bi-clock"></i>
                                                <time datetime="{{ \Illuminate\Support\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d-m-Y') }}">
                                                    {{ \Morilog\Jalali\Jalalian::forge($blog->created_at)->format('%d %B %Y') }}
                                                </time>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="content">
                                        <p>
                                            {{ \Illuminate\Support\Str::limit($blog->shortDescription, 40) }}
                                        </p>
                                    </div>

                                    <div class="read-more mt-auto align-self-end">
                                        <a href="{{ route('singleBlog', $blog->slug) }}">ادامه مطلب</a>
                                    </div>

                                </article>
                            </div><!-- End post list item -->
                        @endforeach
                        </div><!-- End blog posts list -->
                    @else
                        <p class="alert alert-danger">جستجوی بی نتیجه! چیزی یافت نشد.</p>
                    @endif
                    {{ $allBlogs->links('pagination.custom') }}

                </div>

                <div class="col-lg-4">
                    @include('blogSidebar')
                </div>

            </div>

        </div>
    </section><!-- End Blog Section -->
@endsection
