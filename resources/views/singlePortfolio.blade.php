@extends('layout')

@section('title'){{ $project->title }} - نمونه کار@endsection

@section('meta')
    @include('metaTags', ['metadata' => 'project' ,'project' => $project, 'tags' => $tags])
@endsection

@section('content')
    <!-- ======= Breadcrumbs ======= -->
    @include('breadcrumb', ['input' => $project->title])
    <!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
        <div class="container" data-aos="fade-up">

            <div class="row gy-4">

                <div class="col-lg-8">
                    <div class="portfolio-details-slider swiper">
                        <div class="swiper-wrapper align-items-center">

                            <div class="swiper-slide">
                                <img src="{{ url($project->imageURL) }}" alt="{{ $project->title }}">
                            </div>

{{--                            <div class="swiper-slide">--}}
{{--                                <img src="assets/img/portfolio/product-1.jpg" alt="">--}}
{{--                            </div>--}}

{{--                            <div class="swiper-slide">--}}
{{--                                <img src="assets/img/portfolio/branding-1.jpg" alt="">--}}
{{--                            </div>--}}

{{--                            <div class="swiper-slide">--}}
{{--                                <img src="assets/img/portfolio/books-1.jpg" alt="">--}}
{{--                            </div>--}}

                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="portfolio-info">
                        <h3>اطلاعات پروژه</h3>
                        <ul>
                            <li><strong>دسته بندی</strong>: {{ $project->category->title }}</li>
                            <li><strong>مشتری</strong>: {{ $project->client->title }}</li>
                            <li><strong>تاریخ پایان پروژه</strong>: {{ $project->projectDateDone }}</li>
                            <li><strong>آدرس پروژه</strong>: <a href="{{ $project->projectURL }}" target="_blank">{{ $project->projectURL }}</a></li>
                        </ul>
                    </div>
                    <div class="portfolio-description">
                        <h2>توضیحات پروژه</h2>
                        {!! $project->body !!}
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Details Section -->
@endsection
