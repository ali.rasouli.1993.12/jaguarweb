<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function returnLabelStatus($status): string
    {
        if ($status == 1) {
            return '<span class="badge badge-success">فعال</span>';
        }
        return '<span class="badge badge-danger">پیشنویس</span>';
    }

    public function blogs()
    {
        return $this->belongsTo(Blog::class, 'categoryId', 'id');
    }

    public function returnParentNameBlog($id): string
    {
        $cat = Category::where('id', $id)->where('tableName', 'Blog')->first();
        if ($cat) {
            return $cat->title;
        }
        return 'دسته بندی پدر';
    }

    public function returnParentNameProject($id): string
    {
        $cat = Category::where('id', $id)->where('tableName', 'Project')->first();
        if ($cat) {
            return $cat->title;
        }
        return 'دسته بندی پدر';
    }

    public function childCat()
    {
        return $this->hasMany(Category::class, 'parentId', 'id');
//        return Category::where('parentId', $id)->where('status', 1)->where('deleted_at', null)->get();
    }
}
