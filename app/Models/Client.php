<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function returnLabelStatus($status): string
    {
        if ($status == 1) {
            return '<span class="badge badge-success">فعال</span>';
        }
        return '<span class="badge badge-danger">پیشنویس</span>';
    }
}
