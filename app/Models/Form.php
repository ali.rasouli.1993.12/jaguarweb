<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function returnLabelStatus($status): string
    {
        if ($status == 1) {
            return '<span class="badge badge-success">دیده شده</span>';
        }
        return '<span class="badge badge-danger">دیده نشده</span>';
    }
}
