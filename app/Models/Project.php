<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function returnLabelStatus($status): string
    {
        if ($status == 1) {
            return '<span class="badge badge-success">فعال</span>';
        }
        return '<span class="badge badge-danger">پیشنویس</span>';
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Category::class, 'id', 'categoryId');
    }

    public function client(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Client::class, 'id', 'clientId');
    }
}
