<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'categoryId');
    }

    public function scopeSearchBlog($query, $inputSearchText)
    {
        return $query->where('title', 'LIKE' , '%'.$inputSearchText.'%' )->where('status', 1)->where('deleted_at', null)->latest()->paginate(12);
    }

    public function returnLabelStatus($status): string
    {
        if ($status == 1) {
            return '<span class="badge badge-success">فعال</span>';
        }
        return '<span class="badge badge-danger">پیشنویس</span>';
    }
}
