<?php

namespace App\Providers;

use App\Http\Controllers\HomeController;
use App\Http\View\Composers\BlogTagComposer;
use App\Http\View\Composers\CategoryComposer;
use App\Http\View\Composers\RecentPostsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['blogSidebar'], CategoryComposer::class);
        View::composer(['blogSidebar'], RecentPostsComposer::class);
        View::composer(['blogSidebar'], BlogTagComposer::class);
    }
}
