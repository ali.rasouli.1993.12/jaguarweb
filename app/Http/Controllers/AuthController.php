<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function showLogin()
    {
        return view('Admin.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'email.required' => 'ایمیل کاربر الزامی است',
            'password.required' => 'رمز عبور الزامی است',
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', '=', $email)->first();
        if ($user && Hash::check($password, $user->password)) {
            Auth::login($user);
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->route('login')->with('error', 'نام کاربری یا رمز عبور اشتباه است، دوباره سعی کنید.');
        }


    }

    public function showRegister()
    {
        return view('Admin.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'mobile' => 'required|unique:users',
            'password' => 'required|confirmed'
        ] , [
            'name.required' => 'نام کاربر الزامی است',
            'email.required' => 'ایمیل کاربر الزامی است',
            'email.unique' => 'ایمیل کاربر تکراری می باشد',
            'email.email' => 'فرمت ایمیل اشتباه است',
            'mobile.required' => 'تلفن اجباری است',
            'mobile.unique' => 'تلفن تکراری است، دوباره سعی کنید.',
            'password.required' => 'رمز عبور الزامی است',
            'password.confirmed' => 'رمز عبور و تکرار رمز عبور یکسان نیستند',
        ]);

        User::create([
           'name' => $request->input('name'),
           'email' => $request->input('email'),
           'mobile' => $request->input('mobile'),
           'password' => Hash::make($request->input('password')),
           'role' => 'user',
        ]);

        return redirect()->route('login')->with('message', 'ثبت نام کاربر با موفقیت انجام شد.');
    }

    public function logout($id)
    {
        $user = User::where('id', $id)->first();
        if ($user) {
            $user->update([
               'signOut_at' => Carbon::now(),
            ]);
        }
        Auth::logout();
        return redirect()->route('login');
    }
}
