<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;

class CaptchaServiceController extends Controller
{
    public function capthcaFormValidate(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
            'mobile' => 'required',
            'captcha' => 'required|captcha',
        ],[
            'name.required' => 'نوشتن نام و نام خانوادگی الزامی می باشد.',
            'email.required' => 'نوشتن ایمیل الزامی می باشد.',
            'subject.required' => 'نوشتن موضوع پیام الزامی می باشد.',
            'message.required' => 'متن پیام ارسالی الزامی می باشد.',
            'mobile.required' => 'نوشتن شماره موبایل الزامی می باشد.',
            'captcha.required' => 'پرکردن سوال امنیتی الزامی می باشد.',
            'captcha.captcha' => 'پاسخ سوال امنیتی اشتباه می باشد.',
        ]);

        Form::create([
           'name' => $request->input('name'),
           'email' => $request->input('email'),
           'subject' => $request->input('subject'),
           'mobile' => $request->input('mobile'),
           'message' => $request->input('message'),
        ]);
        return back()->with('message', 'پیام شما با موفقیت ثبت شد');
    }
    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_src()]);
    }
}
