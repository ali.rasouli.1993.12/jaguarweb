<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        return view('Admin.Tag.index');
    }

    public function listBlog()
    {
        $allTagListBlog = Tag::where('tableName', '=', 'Blog')->where('deleted_at', null)->latest()->paginate(15);
        return view('Admin.Tag.Blog.list', compact('allTagListBlog'));
    }

    public function createBlog()
    {
        return view('Admin.Tag.Blog.create');
    }

    public function storeBlog(Request $request)
    {
        $this->validate($request, [
           'title' => 'required',
        ], [
            'title.required' => 'وارد کردن نام برچسب الزامی است.'
        ]);

        Tag::create([
            'title' => $request->input('title'),
            'status' => $request->input('status') ? 1 : 0,
            'tableName' => 'Blog'
        ]);

        return redirect()->back()->with('message', 'برچسب بلاگ در سایت ثبت شد.');
    }

    public function editBlog($id)
    {
        $tag = Tag::where('id', $id)->where('tableName', 'Blog')->where('deleted_at', null)->first();
        if ($tag) {
            return view('Admin.Tag.Blog.update', compact('tag'));
        }
        return redirect()->route('admin.tag.blog.list')->with('error', 'برچسب مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function updateBlog(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ], [
            'title.required' => 'وارد کردن نام برچسب الزامی است.'
        ]);
        $tag = Tag::where('id', $id)->where('tableName', 'Blog')->where('deleted_at', null)->first();
        if ($tag) {
            $tag->update([
                'title' => $request->input('title'),
                'status' => $request->input('status') ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.tag.blog.list')->with('message', 'برچسب با موفقیت ویرایش شد.');
        }
        return redirect()->route('admin.tag.blog.list')->with('error', 'برچسب مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function deleteBlog($id)
    {
        $tag = Tag::where('id', $id)->where('tableName', 'Blog')->where('deleted_at', null)->first();
        if ($tag) {
            $tag->update([
               'deleted_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.tag.blog.list')->with('message', 'برچسب با موفقیت از سایت پاک شد.');
        }
        return redirect()->back()->with('error', 'برچسب مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    /*
     * List projects
     */

    public function listProject()
    {
        $allTagListProject = Tag::where('tableName', 'Project')->where('deleted_at', null)->latest()->paginate(15);
        return view('Admin.Tag.Project.list', compact('allTagListProject'));
    }

    public function createProject()
    {
        return view('Admin.Tag.Project.create');
    }

    public function storeProject(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ], [
            'title.required' => 'وارد کردن نام برچسب الزامی است.'
        ]);

        Tag::create([
            'title' => $request->input('title'),
            'status' => $request->input('status') ? 1 : 0,
            'tableName' => 'Project'
        ]);

        return redirect()->route('admin.tag.project.list')->with('message', 'برچسب بلاگ در سایت ثبت شد.');
    }

    public function editProject($id)
    {
        $tag = Tag::where('id', $id)->where('tableName', 'Project')->where('deleted_at', null)->first();
        if ($tag) {
            return view('Admin.Tag.Project.update', compact('tag'));
        }
        return redirect()->route('admin.tag.project.edit')->with('error', 'برچسب مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function updateProject(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ], [
            'title.required' => 'وارد کردن نام برچسب الزامی است.'
        ]);
        $tag = Tag::where('id', $id)->where('tableName', 'Project')->where('deleted_at', null)->first();
        if ($tag) {
            $tag->update([
                'title' => $request->input('title'),
                'status' => $request->input('status') ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.tag.project.list')->with('message', 'برچسب با موفقیت ویرایش شد.');
        }
        return redirect()->route('admin.tag.project.list')->with('error', 'برچسب مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function deleteProject($id)
    {
        $tag = Tag::where('id', $id)->where('tableName', 'Project')->where('deleted_at', null)->first();
        if ($tag) {
            $tag->update([
                'deleted_at' => Carbon::now(),
            ]);
            return redirect()->back()->with('message', 'برچسب با موفقیت از سایت پاک شد.');
        }
        return redirect()->back()->with('error', 'برچسب مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }
}
