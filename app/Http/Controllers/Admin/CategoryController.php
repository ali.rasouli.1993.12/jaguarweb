<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return view('Admin.Category.index');
    }

    /*
     * List blogs
     */

    public function listBlog()
    {
        $allCategoryListBlog = Category::where('tableName', '=', 'Blog')->where('deleted_at', null)->latest()->paginate(15);
        return view('Admin.Category.Blog.list', compact('allCategoryListBlog'));
    }

    public function createBlog()
    {
        $parentCats = Category::where('tableName', 'Blog')->where('deleted_at', null)->where('status', 1)->where('parentId', 0)->get();
        return view('Admin.Category.Blog.create', compact('parentCats'));
    }

    public function storeBlog(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:categories',
            'parentId' => 'required',
        ], [
            'title.required' => 'وارد کردن نام دسته بندی الزامی است.',
            'slug.required' => 'مقدار اسلاگ برای دسته بندی وارد کنید.',
            'slug.unique' => 'اسلاگ مورد نظر برای این دسته بندی تکراری می باشد.',
            'parentId.required' => 'انتخاب دسته بندی پدر الزامی است.',
        ]);

        Category::create([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'parentId' => $request->input('parentId'),
            'status' => $request->input('status') ? 1 : 0,
            'tableName' => 'Blog'
        ]);

        return redirect()->route('admin.category.blog.list')->with('message', 'دسته بندی بلاگ در سایت ثبت شد.');
    }

    public function editBlog($id)
    {
        $cat = Category::where('id', $id)->where('tableName', 'Blog')->where('deleted_at', null)->first();
        $parentCats = Category::where('id', '!=', $id)->where('tableName', 'Blog')->where('deleted_at', null)->where('status', 1)->where('parentId', 0)->get();
        if ($cat) {
            return view('Admin.Category.Blog.update', compact('cat', 'parentCats'));
        }
        return redirect()->route('admin.category.blog.list')->with('error', 'دسته بندی مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function updateBlog(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'parentId' => 'required',
        ], [
            'title.required' => 'وارد کردن نام دسته بندی الزامی است.',
            'parentId.required' => 'انتخاب دسته بندی پدر الزامی است.',
        ]);
        $tag = Category::where('id', $id)->where('tableName', 'Blog')->where('deleted_at', null)->first();
        if ($tag) {
            $tag->update([
                'title' => $request->input('title'),
                'parentId' => $request->input('parentId'),
                'status' => $request->input('status') ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.category.blog.list')->with('message', 'دسته بندی با موفقیت ویرایش شد.');
        }
        return redirect()->route('admin.category.blog.list')->with('error', 'دسته بندی مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function deleteBlog($id)
    {
        $cat = Category::where('id', $id)->where('tableName', 'Blog')->where('deleted_at', null)->first();
        if ($cat) {
            $cat->update([
                'slug' => $cat->slug . '-' . $cat->id,
                'deleted_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.category.blog.list')->with('message', 'دسته بندی با موفقیت از سایت پاک شد.');
        }
        return redirect()->route('admin.category.blog.list')->with('error', 'دسته بندی مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    /*
     * List projects
     */

    public function listProject()
    {
        $allCategoryListProject = Category::where('tableName', 'Project')->where('deleted_at', null)->latest()->paginate(15);
        return view('Admin.Category.Project.list', compact('allCategoryListProject'));
    }

    public function createProject()
    {
        $parentCats = Category::where('tableName', 'Project')->where('deleted_at', null)->where('status', 1)->where('parentId', 0)->get();
        return view('Admin.Category.Project.create', compact('parentCats'));
    }

    public function storeProject(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:categories',
            'parentId' => 'required',
        ], [
            'title.required' => 'وارد کردن نام دسته بندی الزامی است.',
            'slug.required' => 'مقدار اسلاگ برای دسته بندی وارد کنید.',
            'slug.unique' => 'اسلاگ مورد نظر برای این دسته بندی تکراری می باشد.',
            'parentId.required' => 'انتخاب دسته بندی پدر الزامی است.',
        ]);


        Category::create([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'parentId' => $request->input('parentId'),
            'status' => $request->input('status') ? 1 : 0,
            'tableName' => 'Project'
        ]);

        return redirect()->route('admin.category.project.list')->with('message', 'دسته بندی بلاگ در سایت ثبت شد.');
    }

    public function editProject($id)
    {
        $cat = Category::where('id', $id)->where('tableName', 'Project')->where('deleted_at', null)->first();
        $parentCats = Category::where('id', '!=', $id)->where('tableName', 'Project')->where('deleted_at', null)->where('status', 1)->where('parentId', 0)->get();
        if ($cat) {
            return view('Admin.Category.Project.update', compact('cat', 'parentCats'));
        }
        return redirect()->route('admin.category.project.list')->with('error', 'دسته بندی مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function updateProject(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'parentId' => 'required',
        ], [
            'title.required' => 'وارد کردن نام دسته بندی الزامی است.',
            'parentId.required' => 'انتخاب دسته بندی پدر الزامی است.',
        ]);
        $cat = Category::where('id', $id)->where('tableName', 'Project')->where('deleted_at', null)->first();
        if ($cat) {
            $cat->update([
                'title' => $request->input('title'),
                'parentId' => $request->input('parentId'),
                'status' => $request->input('status') ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.category.project.list')->with('message', 'دسته بندی با موفقیت ویرایش شد.');
        }
        return redirect()->route('admin.category.project.list')->with('error', 'دسته بندی مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }

    public function deleteProject($id)
    {
        $cat = Category::where('id', $id)->where('tableName', 'Project')->where('deleted_at', null)->first();
        if ($cat) {
            $cat->update([
                'slug' => $cat->slug . '-' . $cat->id,
                'deleted_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.category.project.list')->with('message', 'دسته بندی با موفقیت از سایت پاک شد.');
        }
        return redirect()->route('admin.category.project.list')->with('error', 'دسته بندی مورد نظر یافت نشد. با پشتیبانی تماس بگیرید.');
    }
}
