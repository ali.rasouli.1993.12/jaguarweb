<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Category;
use App\Models\Project;
use App\Models\Client;
use Carbon\Carbon;


class ProjectController extends Controller
{
    private $allTags = [];
    private $allCats = [];
    private $allClients = [];

    public function __construct()
    {
        $this->allTags = Tag::where('tableName', 'Project')->where('deleted_at', null)->where('status', 1)->latest()->get();
        $this->allCats = Category::where('tableName', 'Project')->where('deleted_at', null)->where('status', 1)->latest()->get();
        $this->allClients = Client::where('tableName', 'Project')->where('deleted_at', null)->where('status', 1)->latest()->get();
    }

    /*
     * Show all Projects
     */
    public function index() {
        $allProjects = Project::where('deleted_at', null)->where('status', 1)->latest()->paginate(15);
        return view('Admin.Project.index', compact('allProjects'));
    }

    /*
     * Show create Project page
     */
    public function create() {
        return view('Admin.Project.create', ['allTags'=>$this->allTags, 'allCats'=>$this->allCats, 'allClients' => $this->allClients]);
    }

    public function insert(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:projects',
            'body' => 'required',
            'clientId' => 'required',
            'projectURL' => 'required',
            'projectDateDone' => 'required',
            'categoryId' => 'required',
        ], [
            'title.required' => 'عنوان پروژه الزامی می باشد.',
            'slug.required' => 'اسلاک برای پروژه الزامی می باشد.',
            'slug.unique' => 'اسلاگ برای پروژه در سایت موجود می باشد.',
            'body.required' => 'محتوای توضیحات پروژه الزامی می باشد.',
            'clientId.required' => 'انتخاب مشتری برای پروژه الزامی می باشد.',
            'projectURL.required' => 'وارد کردن آدرس پروژه الزامی می باشد.',
            'projectDateDone.required' => 'وارد کردن تاریخ پایان پروژه الزامی می باشد.',
            'categoryId.required' => 'انتخاب دسته بندی برای پروژه الزامی می باشد.',
        ]);
        $tags = $request->input('tagsId') ? serialize($request->input('tagsId')) : Null;
        $fileName = '';
        if ($request->has('imageURL')) {
            $originName = $request->file('imageURL')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('imageURL')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('imageURL')->move('projects', $fileName);
            $fileName = 'projects/'. $fileName;
        }
        Project::create([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'body' => $request->input('body'),
            'clientId' => $request->input('clientId'),
            'projectURL' => $request->input('projectURL'),
            'imageURL' => $fileName?? '',
            'projectDateDone' => $request->input('projectDateDone'),
            'categoryId' => $request->input('categoryId'),
            'shortDescription' => $request->input('shortDescription') ?? Null,
            'tagsId' => $tags,
            'status' => $request->input('status') ? 1 : 0,
        ]);
        return redirect()->route('admin.project.index')->with('message', 'پروژه جدید با موفقیت ایجاد شد.');
    }

    public function edit($id) {
        $project = Project::where('id', $id)->where('deleted_at', null)->first();
        if ($project) {
            return view('Admin.Project.update', ['project'=>$project, 'allTags'=>$this->allTags, 'allCats'=>$this->allCats, 'allClients' => $this->allClients]);
        }
        return redirect()->route('admin.project.index')->with('error', 'نمونه کار مورد نظر پیدا نشد. دوباره بررسی کنید.');
    }

    public function update(Request $request, $id) {
        $hasImage = $request->has('imageURL');
        $project = Project::where('id', $id)->where('deleted_at',null)->first();
        if ($project) {
            $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
                'slug' => 'required',
                'clientId' => 'required',
                'projectURL' => 'required',
                'projectDateDone' => 'required',
                'categoryId' => 'required',
            ], [
                'title.required' => 'عنوان پروژه الزامی می باشد.',
                'body.required' => 'محتوای توضیحات پروژه الزامی می باشد.',
                'clientId.required' => 'انتخاب مشتری برای پروژه الزامی می باشد.',
                'projectURL.required' => 'وارد کردن آدرس پروژه الزامی می باشد.',
                'projectDateDone.required' => 'وارد کردن تاریخ پایان پروژه الزامی می باشد.',
                'categoryId.required' => 'انتخاب دسته بندی برای پروژه الزامی می باشد.',
            ]);

            $tags = $request->input('tagsId') ? serialize($request->input('tagsId')) : Null;
            $fileName = '';
            if ($request->has('imageURL')) {
                $originName = $request->file('imageURL')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('imageURL')->getClientOriginalExtension();
                $fileName = $fileName.'_'.time().'.'.$extension;
                $request->file('imageURL')->move('projects', $fileName);
                $fileName = 'projects/'. $fileName;
            }

            $project->update([
                'title' => $request->input('title'),
                'body' => $request->input('body'),
                'slug' => $request->input('slug'),
                'clientId' => $request->input('clientId'),
                'projectURL' => $request->input('projectURL'),
                'imageURL' => $hasImage ? $fileName : $project->imageURL,
                'projectDateDone' => $request->input('projectDateDone'),
                'categoryId' => $request->input('categoryId'),
                'shortDescription' => $request->input('shortDescription') ?? Null,
                'tagsId' => $tags,
                'status' => $request->input('status') ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.project.index')->with('message', 'پروژه با موفقیت ویرایش شد.');
        }
        return redirect()->route('admin.project.index')->with('error', 'نمونه کار مورد نظر پیدا نشد. دوباره بررسی کنید.');
    }
}
