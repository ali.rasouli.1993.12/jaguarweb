<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use Carbon\Carbon;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $allClients = Client::where('tableName', 'Project')->where('deleted_at', null)->latest()->paginate(15);
        return view('Admin.Client.index', compact('allClients'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('Admin.Client.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ], [
            'title.required' => 'وارد کردن نام مشتری الزامی است.',
        ]);

        Client::create([
            'title' => $request->input('title'),
            'tableName' => 'Project',
            'status' => $request->input('status') ? 1 : 0,
        ]);

        return redirect()->route('admin.client.index')->with('message', 'مشتری جدید در سایت ثبت شد');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $client = Client::where('id', $id)->where('deleted_at', null)->first();
        return view('admin.client.update', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ], [
            'title.required' => 'وارد کردن نام مشتری الزامی است.',
        ]);
        $client = Client::where('id', $id)->where('deleted_at', null)->first();
        if ($client) {
            $client->update([
                'title' => $request->input('title'),
                'status'=> $request->input('status') ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);
            return redirect()->route('admin.client.index')->with('message', 'مشتری جدید با موفقیت در سایت ثبت شد');
        }
        return redirect()->route('admin.client.index')->with('error', 'مشکل در به روزرسانی مشتری، لطفا دوباره سعی کنید');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $client = Client::where('id', $id)->where('deleted_at', null)->first();
        if ($client) {
            $client->delete();
            return redirect()->route('admin.client.index')->with('message', 'مشتری از سایت حذف شد');
        }
        return redirect()->route('admin.client.index')->with('error', 'مشکل در به پاک کردن مشتری، لطفا دوباره سعی کنید');
    }
}
