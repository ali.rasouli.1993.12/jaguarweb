<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $allTags = [];
    private $allCats = [];

    public function __construct()
    {
        $this->allTags = Tag::where('tableName', 'Blog')->where('deleted_at', null)->where('status', 1)->latest()->get();
        $this->allCats = Category::where('tableName', 'Blog')->where('deleted_at', null)->where('status', 1)->latest()->get();
    }

    /*
     * Show all Blogs
     */
    public function index()
    {
        $allBlogs = Blog::where('deleted_at', null)->with('category')->latest()->paginate(10);
        return view('Admin.Blog.index', compact('allBlogs'));
    }

    /*
     * Show create blog page
     */
    public function create()
    {
        return view('Admin.Blog.create',['allTags' => $this->allTags, 'allCats' => $this->allCats]);
    }

    /*
     * insert blog to database
     */
    public function insert(Request $request)
    {
        $this->validate($request, [
            'slug' => 'unique:blogs|required',
            'title' => 'required',
            'imageURL' => 'required',
            'body' => 'required',
            'shortDescription' => 'max:255',
            'categoryId' => 'required',
        ], [
            'slug.unique' => 'اسلاگ برای این پست، تکراری می باشد',
            'slug.required' => 'اسلاگ برای این پست، الزامی می باشد',
            'title.required' => 'عنوان برای این پست، الزامی می باشد',
            'imageURL.required' => 'انتخاب عکس برای این پست، الزامی می باشد',
            'body.required' => 'متن نوشته برای این پست، الزامی می باشد',
            'categoryId.required' => 'انتخاب دسته بندی برای این پست، الزامی می باشد',
            'shortDescription.max' => 'متن کوتاه بیشتر از 255 کاراکتر نمیتواند باشد',
        ]);
        $tags = $request->input('tagsId') ? serialize($request->input('tagsId')) : Null;
        $fileName = '';
        if ($request->has('imageURL')) {
            $originName = $request->file('imageURL')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('imageURL')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('imageURL')->move('images', $fileName);
            $fileName = 'images/'. $fileName;
        }
        Blog::create([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'body' => $request->input('body'),
            'categoryId' => $request->input('categoryId'),
            'imageURL' => $fileName?? '',
            'tagsId' => $tags,
            'status' => $request->input('status') ? 1 : 0,
            'shortDescription' => $request->input('shortDescription'),
        ]);
        return redirect()->route('admin.blog.index')->with('message', 'پست شما با موفقیت در سایت ثبت شد.');
    }

    /*
     * Show edit blog page
     */
    public function edit($id)
    {
        $blog = Blog::where('id', $id)->where('deleted_at', null)->first();
        if ($blog) {
            return view('Admin.Blog.update', ['blog' => $blog, 'allTags' => $this->allTags, 'allCats' => $this->allCats]);
        }
        return redirect()->route('admin.blog.index')->with('error','پست یا مقاله مورد نظر یافت نشد. دوباره سعی کنید.');
    }

    /*
     * update blog to database
     */
    public function update(Request $request, $id)
    {
        $hasImage = $request->has('imageURL');
        $blog = Blog::where('id', $id)->where('deleted_at', null)->first();
        if ($blog) {
            $this->validate($request, [
                'slug' => 'required',
                'title' => 'required',
                'body' => 'required',
                'categoryId' => 'required',
                'shortDescription' => 'max:255',
            ], [
                'slug.required' => 'اسلاگ برای این پست، الزامی می باشد',
                'title.required' => 'عنوان برای این پست، الزامی می باشد',
                'body.required' => 'متن نوشته برای این پست، الزامی می باشد',
                'categoryId.required' => 'انتخاب دسته بندی برای این پست، الزامی می باشد',
                'shortDescription.max' => 'متن کوتاه بیشتر از 255 کاراکتر نمیتواند باشد',
            ]);
            $tags = $request->input('tagsId') ? serialize($request->input('tagsId')) : Null;
            $fileName = '';
            if ($request->has('imageURL')) {
                $originName = $request->file('imageURL')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('imageURL')->getClientOriginalExtension();
                $fileName = $fileName.'_'.time().'.'.$extension;
                $request->file('imageURL')->move('images', $fileName);
                $fileName = 'images/'. $fileName;
            }
            $blog->update([
                'title' => $request->input('title'),
                'slug' => $request->input('slug'),
                'body' => $request->input('body'),
                'categoryId' => $request->input('categoryId'),
                'imageURL' => $hasImage ? $fileName : $blog->imageURL,
                'tagsId' => $tags,
                'status' => $request->input('status') ? 1 : 0,
                'shortDescription' => $request->input('shortDescription'),
                'updated_at' => Carbon::now()
            ]);
            return redirect()->route('admin.blog.index')->with('message', 'پست شما با موفقیت ویرایش شد.');
        }
        return redirect()->route('admin.blog.index')->with('error','پست یا مقاله مورد نظر یافت نشد. دوباره سعی کنید.');
    }

    /*
     * delete the selected blog
     */
    public function delete($id)
    {
        $blog = Blog::where('id', $id)->where('deleted_at', null)->first();
        if ($blog) {
            $blog->update([
                'deleted_at' => Carbon::now()
            ]);
            return redirect()->route('admin.blog.index')->with('message', 'پست شما با موفقیت از سایت پاک شد.');
        }
        return redirect()->route('admin.blog.index')->with('error','پست یا مقاله مورد نظر یافت نشد. دوباره سعی کنید.');
    }

    /*
     * Upload Image for body blog
     */
    public function uploadImage(Request $request)
    {
        if ($request->has('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move('images', $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName);
            $msg = 'Image successfully uploaded' . 'the URL is :' . $url;
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }
}
