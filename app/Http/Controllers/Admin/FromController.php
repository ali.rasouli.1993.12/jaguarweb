<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Form;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FromController extends Controller
{
    public function index()
    {
        $forms = Form::where('deleted_at', null)->latest()->paginate(10);
        return view('Admin.Form.index', compact('forms'));
    }

    public function show($id)
    {
        $form = Form::where('id', $id)->where('deleted_at', null)->first();
        if (!$form) {
            return redirect()->route('admin.form.index')->with('error', 'فرم مورد نظر در سایت موجود نیست، دوباره امتحان کنید.');
        }
        $form->update([
            'visited' => 1,
        ]);
        return view('Admin.Form.show', compact('form'));
    }

    public function destroy($id)
    {
        $form = Form::where('id', $id)->where('deleted_at', null)->first();
        if (!$form) {
            return redirect()->route('admin.form.index')->with('error', 'فرم مورد نظر در سایت موجود نیست، دوباره امتحان کنید.');
        }
        $form->update([
            'deleted_at' => Carbon::now(),
        ]);
        return redirect()->route('admin.form.index')->with('message', 'پیام با موفقیت از سایت پاک شد.');
    }
}
