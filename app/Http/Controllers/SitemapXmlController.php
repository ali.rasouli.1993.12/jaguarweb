<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class SitemapXmlController extends Controller
{
    public function index()
    {
        $blogs = Blog::where('status', 1)->where('deleted_at', null)->get();
        return response()->view('sitemap', [
            'posts' => $blogs
        ])->header('Content-Type', 'text/xml');
    }
}
