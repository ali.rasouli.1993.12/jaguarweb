<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Blog;
use App\Models\Tag;
use App\Models\Category;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    public function home()
    {
        $blogs = Blog::where('deleted_at', null)->where('status', 1)->limit(3)->latest()->get();
        $portfolioCats = Category::where('deleted_at',)->where('status', 1)->where('tableName', '=', 'Project')->latest()->get();
        $projects = Project::where('deleted_at', null)->where('status', 1)->latest()->limit(100)->get();
        return view('home', ['blogs' => $blogs, 'projects' => $projects, 'portfolioCats' => $portfolioCats]);
    }

    public function portfolio()
    {
        $portfolios = Project::where('status', 1)->where('deleted_at', null)->latest()->paginate(12);
        return view('portfolio', compact('portfolios'));
    }

    public function singlePortfolio($slug)
    {
        $project = Project::where('slug', $slug)->where('deleted_at', null)->where('status', 1)->with('category', 'client')->first();
        if ($project) {
            $tags = [];
            if ($project->tags) {
                foreach (unserialize($project->tagsId) as $item) {
                    $Tag = Tag::where('id', $item)->where('deleted_at', null)->first();
                    if ($Tag) {
                        array_push($tags, $Tag);
                    }
                }
            }
            return view('singlePortfolio', compact('project', 'tags'));
        } else {
            return view('404');
        }
    }

    public function blog()
    {
        $allBlogs = Blog::where('deleted_at', null)->where('status', 1)->latest()->paginate(12);
        return view('blog', compact('allBlogs'));
    }

    public function singleBlog($slug)
    {
        $blog = Blog::where('slug', $slug)->where('deleted_at', null)->where('status', 1)->with('category')->first();
        if ($blog) {
            $tags = [];
            foreach (unserialize($blog->tagsId) as $item) {
                $Tag = Tag::where('id', $item)->where('deleted_at', null)->first();
                if ($Tag) {
                    array_push($tags, $Tag);
                }
            }
            return view('singleBlog', compact('blog', 'tags'));
        } else {
            return view('errors.404');
        }
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required',
        ] , [
            'search.required' => 'وارد کردن متن جستجو الزامی است'
        ]);
        $searchText = $request->input('search');
        $allBlogs = Blog::SearchBlog($searchText);
        return view('search', compact('allBlogs', 'searchText'));
    }
}
