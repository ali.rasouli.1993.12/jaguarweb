<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function showCategory($slug)
    {
        $cat = Category::where('slug', $slug)->where('status', 1)->where('deleted_at', null)->first();
        if ($cat) {
            $child = [];
            if ($cat->parentId == 0) {
                $child = Category::where('parentId', $cat->id)->get();
                // return $child;
            }
             $allBlogs = Blog::where('status', 1)->where('deleted_at', null)->where('categoryId', $cat->id)->latest()->paginate(12);
//            $allBlogs = Blog::where('status', 1)->where('deleted_at', null)->whereHas('categoryId', function($query) use ($child){
//                $query->whereIn('child.id',$child);
//            })->latest()->paginate(12);
//            return $allBlogs;
            return view('category', compact('allBlogs', 'cat'));
        }
        return view('errors.404');

    }
}
