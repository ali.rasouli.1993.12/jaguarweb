<?php


namespace App\Http\View\Composers;


use App\Models\Tag;
use Illuminate\View\View;

class BlogTagComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('tags', Tag::where('status', 1)->where('deleted_at', null)->limit(15)->latest()->get());
    }
}
