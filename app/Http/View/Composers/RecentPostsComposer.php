<?php

namespace App\Http\View\Composers;

use App\Models\Blog;
use Illuminate\View\View;

class RecentPostsComposer
{

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('recentBlogs', Blog::where('status', 1)->where('deleted_at', null)->limit(5)->latest()->get());
    }
}

